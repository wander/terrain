## Terrain (HDRP)

Builds up a terrain using PDOK BGT information.


### Dependencies (GIT Packages)
#### optional
- 3dbuildings
- trees

#### mandatory
- vectortile
- geotiff
- utils
### Usage

Sweep the TerrainBuilder prefab into the Hierarchy and press 'Build' on the TerrainBuilder script.
To also have buildings and trees, the according packages must be added too.
### Samples


#### Nijmegen
![Image](./Samples/Nijmegen_2.png)
#### Nijmegen Waalbridge
![Image](./Samples/Nijmegen.png)
#### Rhenen
![Image](./Samples/Rhenen.png)
#### Amsterdam
![Image](./Samples/Amsterdam.png)
#### Rotterdam
![Image](./Samples/Rotterdam.png)
#### Scheveningen
![Image](./Samples/Scheveningen.png)
#### Biesbosch
![Image](./Samples/Biesbosch.png)
#### Dunes
![Image](./Samples/Dunes.png)
#### Winter
![Image](./Samples/Winter.png)
