using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.ResourceManagement.AsyncOperations;


namespace Wander
{
    [ExecuteInEditMode] // Need this for SendMessage to be reveived without error in Edit mode.
    public class TileStreamer : MonoBehaviour
    {
        public class TileRequest
        {
            public bool loadFailed;
            public float time;
            public GameObject tile;
            public bool isAsyncProcessRunning;
            public bool isResourceLoadStarted;
            public AsyncOperationHandle<GameObject> resource;
        }

        [ReadOnly] public double originRDX;
        [ReadOnly] public double originRDY;
        [ReadOnly] public float tileSize;
        [ReadOnly] public Vector2Int tileOrigin;
        [ReadOnly] public GameObject tileRoot;
        [ReadOnly] public WaterSurface water;
        [ReadOnly] public string areaName = "Steenbergen";

        public int pixelError = 4;
        public Camera targetCam;
        public Vector2 wgs84Origin;
        public int zoom;
        public int numRings = 2;
        public float removeDelay = 7;

        
        private Dictionary<Vector2Int, TileRequest> tileRequests = new();

        public void SetGPSCoord( Dictionary<string, string> boundaryData )
        {
            var wgs84Lat = double.Parse( boundaryData["gpsx"] );
            var wgs84Lon = double.Parse( boundaryData["gpsy"] );
            wgs84Origin  = new Vector2( (float)wgs84Lat, (float) wgs84Lon );
            tileSize     = float.Parse( boundaryData["tileSize"] );
            zoom         = int.Parse( boundaryData["zoom"] );
            areaName     = boundaryData["areaName"];

            // Derived
            RDUtils.GPS2RD( wgs84Lat, wgs84Lon, out originRDX, out originRDY );
            tileOrigin = RDUtils.RD2Tile( originRDX, originRDY, zoom );

#if UNITY_EDITOR
            EditorUtility.SetDirty( this );
#endif
        }

        private void Start()
        {
            if (Application.isPlaying)
            {
                if ( targetCam == null )
                {
                    targetCam = Camera.main;
                }
                tileRoot = new GameObject( "Tiles_Root_" + zoom );
                tileRoot.transform.position = new Vector3( 0, -100, 0 );
                var requestedCentreTile = RDUtils.RD2Tile( originRDX, originRDY, zoom );
                var requestedCentreTileRD = RDUtils.Tile2RD( requestedCentreTile, zoom );
                (double x, double y) offset = ( requestedCentreTileRD.rdX - originRDX, requestedCentreTileRD.rdY - originRDY ); // Offset is calcaulated from smallest, so that tiles align with bigger versions correctly.
                tileRoot.transform.position = new Vector3( (float)offset.x, -100, (float)offset.y );
                InitializeWater();
            }
        }

        void InitializeWater()
        {
            water = tileRoot.AddComponent<WaterSurface>();
            water.geometryType = WaterGeometryType.Custom;
            water.ripplesChaos = 0;
            water.ripplesWindSpeed = 11;
            water.largeChaos = 0.2f;
            water.largeBand0Multiplier = 0;
            water.largeBand1Multiplier = 0;
            water.timeMultiplier = 0.4f;
            water.maxRefractionDistance = 0.3f;
            water.absorptionDistance = 4;
            water.tessellation = false;
            water.deformation = false;
            water.caustics = false;
            water.underWater = false;
        }

        private void Update()
        {
            if (Application.isPlaying)
            {
                if ( targetCam != null )
                {
                    transform.position = targetCam.transform.position;
                }
                LoadDesiredTiles();
                RemoveOutdatedTiles();
            }
        }

        void LoadIfIsNewTile( Vector2Int centre, int dx, int dy )
        {
            var coord = centre + new Vector2Int(dx, dy);
            if (!tileRequests.TryGetValue( coord, out var tileRequest ))
            {
                string path = $"Assets/{areaName}/tiles_{ zoom}/tile_{coord.x}_{coord.y}/tile.prefab".FS();
                tileRequest = new();
                tileRequest.time = Time.time;
                tileRequest.isAsyncProcessRunning = true;
                tileRequests.Add( coord, tileRequest );
                Addressables.LoadResourceLocationsAsync( path, typeof( GameObject ) ).Completed += ( location ) =>
                {
                    if (!location.IsValid() || location.Result.Count <= 0) // This actually happens: valid, yet it is empty.
                    {
                        tileRequest.loadFailed = true;
                        tileRequest.isAsyncProcessRunning = false;
                        return;
                    }

                    tileRequest.resource = Addressables.LoadAssetAsync<GameObject>( location.Result[0] );
                    tileRequest.isResourceLoadStarted = true;
                    tileRequest.resource.Completed += ( resource ) =>
                    {
                        if (!resource.IsValid())
                        {
                            tileRequest.loadFailed = true;
                            tileRequest.isAsyncProcessRunning = false;
                            return;
                        }

                        InstantiateAsync( resource.Result ).completed += ( asyncOp ) => 
                        {
                            tileRequest.isAsyncProcessRunning = false;

                            if ((asyncOp as AsyncInstantiateOperation<GameObject>).Result.Length <= 0)
                            {
                                tileRequest.loadFailed = true;
                                return;
                            }

                            GameObject tile  = (asyncOp as AsyncInstantiateOperation<GameObject>).Result[0];
                            tileRequest.tile = tile;

                            if ( tileRoot == null ) // This happens when the creation was initiated, but PIE was stopped and creation did not finish yet.
                            {
                                CleanRequest( tileRequest );
                                tileRequests.Remove( coord );
                                return;
                            }

                            tile.name = $"Tile_{coord.x}_{coord.y}";
                            tile.GetComponent<Terrain>().heightmapPixelError = pixelError;
                            tile.transform.SetParent( tileRoot.transform, true );
                            tile.transform.localPosition = new Vector3( (coord.x - tileOrigin.x), 0, (tileOrigin.y - coord.y) ) * tileSize;
                            if (tile.transform.childCount > 0) // Water is first child.
                            {
                                var waterMesh = tile.transform.GetChild( 0 );
                                var render = waterMesh.GetComponent<MeshRenderer>();
                                water.meshRenderers.Add( render );
                                render.enabled = false;
                            }
                        };
                    };
                };
            }
            else
            {
                tileRequest.time = Time.time;
            }
        }

        void LoadDesiredTiles()
        {
            var rdX = originRDX + transform.position.x;
            var rdY = originRDY + transform.position.z;
            var centreTile = RDUtils.RD2Tile( rdX, rdY, zoom );
            LoadIfIsNewTile( centreTile, 0, 0 );
            for (int c = 1;c <= numRings;c++)
            {
                for (int y2 = -c;y2 <= c;y2++)
                {
                    LoadIfIsNewTile( centreTile, -c, y2 );
                }
                for (int y2 = -c;y2 <= c;y2++)
                {
                    LoadIfIsNewTile( centreTile, c, y2 );
                }
                for (int x2 = -c+1;x2 <= c-1;x2++)
                {
                    LoadIfIsNewTile( centreTile, x2, c );
                }
                for (int x2 = -c+1;x2 <= c-1;x2++)
                {
                    LoadIfIsNewTile( centreTile, x2, -c );
                }
            }
        }

        void RemoveOutdatedTiles()
        {
            var removeList = new List<Vector2Int>();
            foreach( var kvp in tileRequests )
            {
                var tileRequest = kvp.Value;
                if ( tileRequest.time - Time.time < -removeDelay && (!tileRequest.isAsyncProcessRunning || tileRequest.loadFailed) )
                {
                    removeList.Add( kvp.Key );
                }
            }
            removeList.ForEach( coord =>
            {
                if ( tileRequests.TryGetValue( coord, out var tileRequest ) )
                {
                    CleanRequest( tileRequest );
                    tileRequests.Remove( coord );
                }
            } );
        }

        void RemoveWater(TileRequest request)
        {
            var tile = request.tile;
            if (tile != null && tile.transform.childCount != 0) // Remove water
            {
                var waterGo  = tile.transform.GetChild( 0 );
                var renderer = waterGo.GetComponent<MeshRenderer>();
                water.meshRenderers.Remove( renderer );
            }
        }

        void CleanRequest(TileRequest request)
        {
            if (request == null)
                return;
            RemoveWater( request );
            if (request.isResourceLoadStarted)
            {
                Addressables.Release( request.resource );
            }
            request.tile.Destroy();
        }
    }

#if UNITY_EDITOR
    [CustomEditor( typeof( TileStreamer ) )]
    [InitializeOnLoad]
    public class TileStreamerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var streamer = target as TileStreamer;
            DrawDefaultInspector();

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Validate" ))
                {
                }
                if (GUILayout.Button( "Delete invalid" ))
                {
                }
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            {
            }
            GUILayout.EndHorizontal();
        }
    }
#endif
}