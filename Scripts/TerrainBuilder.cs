using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.HighDefinition;
using Material = UnityEngine.Material;



#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;

namespace Wander
{
    public enum RenderType
    {
        Rasterize,
        Raytrace
    }

    public enum WaterType
    {
        None,
        Tiles,
        Islands,
    }

    [Serializable]
    public class TerrainLayer2
    {
        public Texture2D mainTexture;
        public Texture2D microTexture1;
        public Texture2D microTexture2;
        public Texture2D normalMap;
        public float metallic = 0.1f;
        public float smoothness = 0.03f;
        public float tiling = 0.1f;
        public List<string> matchingNames;
        public List<GameObject> foliage;
        [Range(0, 1)] public float foliageDensity = 1;
        public bool foliageAsPrefabs;
    }   

    [ExecuteAlways()]
    public class TerrainBuilder : MonoBehaviour
    {
        [ReadOnly] public WaterSurface water;
        [ReadOnly] public double originRDX;
        [ReadOnly] public double originRDY;
        [ReadOnly] public double tileSize;
        [ReadOnly] public double offsetX;
        [ReadOnly] public double offsetY;
        [ReadOnly] public double boundsSize;
        [ReadOnly] public int vtileResolution;
        [ReadOnly] public int numVtilesPerTile;
        [ReadOnly] public int numVtilesPerWide;
        [ReadOnly] public int totalNumVTiles;
        [ReadOnly] public int vTileZoom = 12;
        [ReadOnly] public List<List<string>> layerNamesList;

        [Header("General")]
        public RenderType renderType        = RenderType.Rasterize;
        public bool runOnStart              = false;
        public string prefix                = "Terrain";
        public float pixelError             = 2;
        public Vector2 originWGS84          = new Vector2(51.985365f, 5.664941f); // Copy directly from Google maps. This is Forum on Campus WUR.
        public int numRings                 = 1;
        public int zoom                     = 12;
        public int controlResolution        = 4096;
        public int heightMapResolution      = 512;
        public bool loadHeight              = true;
        public bool filterHeight            = true;
        public float terrainHeight          = 200;
        public bool autoRefreshMaterials    = false;
        public LayerMask groundLayer;

        [Header("Tile Generation")]
        public bool isGenerating            = false;
        public bool overwriteExistingTiles  = false;
        public bool showTilesInEditor       = true;
        public Vector2Int buildSpecific     = new Vector2Int(-1,-1);

        [Header("Water Elevation")]
        public bool applyWaterElevation     = true;
        public byte waterLayerIdx           = 0;
        public int  numRingsToSearch        = 15;
        public float deltaDepthWaterPixel   = 0.0045f;

        [Header("Water")]
        public WaterType WaterType          = WaterType.Islands;
        public float waterTileSize          = 3;
        public float waterHeightOffset      = 0;
        public float waterIslandCapScale    = 3.25f;
        public bool  waterCombineMeshes     = true;

        [Header("Foliage")]
        public bool createFoliage           = false;
        public float foliageGlobalDensity   = 0.03f;
        public int foliageScanShift         = 2;
        public float foliageLODSize         = 10;

        [Header("Downloads")]
        public int maxDownloads             = 16;

        [Header("Shading")]        
        public Material terrainMat4         = null;
        public Material terrainMat8         = null;
        public Material terrainMat10        = null;
        public List<TerrainLayer2> layers;

        internal int numActiveVtiles         = 0;
        internal int numPrevActiveVTiles     = 0;
        internal int numVTilesFinished       = 0;
        internal int numPrevVTilesFinished   = 0;
        internal int numStartedTiles         = 0;
        internal int numFinishedTiles        = 0;
        private List<GameObject> tiles       = new List<GameObject>();

        public OpenTK.Mathematics.Vector3d[] VTileClippingVertices { get; private set; } 
        public int[] VTileClippingIndices { get; private set; }

        private EditorCoroutine buildingRoutine;
        internal bool issuedBuild;
        internal bool issuedSyncCoords;

        internal void Cancel()
        {
            if (buildingRoutine != null)
            {
                EditorCoroutineUtility.StopCoroutine( buildingRoutine );
                buildingRoutine = null;
            }
            StopAllCoroutines();
            tiles?.ForEach( t =>
            {
                if(t!=null) t.GetComponent<TerrainTile>()?.CancelAndClean();
            } );
        }

        public void SyncCoords()
        {
            var sceneObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            var boundaryData = new Dictionary<string, string>();

            tileSize = RDUtils.CalcTileSizeRD( zoom );
            boundaryData["gpsx"] = originWGS84.x.ToString();
            boundaryData["gpsy"] = originWGS84.y.ToString();
            boundaryData["zoom"] = zoom.ToString();
            boundaryData["tileSize"] = RDUtils.CalcTileSizeRD( zoom ).ToString();
            boundaryData["numRings"] = numRings.ToString();

            double rdX, rdY;
            RDUtils.GPS2RD( originWGS84.x, originWGS84.y, out rdX, out rdY );
            var requestedCentreTile   = RDUtils.RD2Tile( rdX, rdY, zoom );
            var requestedCentreTileRD = RDUtils.Tile2RD( requestedCentreTile, zoom );
            (double x, double y) offset = ( requestedCentreTileRD.rdX - rdX, requestedCentreTileRD.rdY - rdY );
            offsetX = offset.x;
            offsetY = offset.y;
            boundaryData["offsetx"] = offsetX.ToString();
            boundaryData["offsety"] = offsetY.ToString();
            
            boundsSize  = (numRings*2+1)*tileSize; // (NumRings*2+1) is nTiles wide.
            boundaryData["boundsSize"] = boundsSize.ToString();

            foreach ( var sceneObject in sceneObjects )
            {
                sceneObject.SendMessage( "SetGPSCoord", boundaryData, SendMessageOptions.DontRequireReceiver ); 
            }
#if UNITY_EDITOR
            EditorUtility.SetDirty( this );
#endif
        }

        public void Update()
        {
            if (issuedSyncCoords)
            {
                issuedSyncCoords=false;
                SyncCoords();
            }

            if (issuedBuild)
            {
                issuedBuild=false;
                Build();
            }
        }

        public void Build()
        {
            Cancel();
            CheckPreliminaries();
            CreateLayerNamesList();
            buildingRoutine = EditorCoroutineUtility.StartCoroutine( CreateTerrain(), this );
        }

        internal void FetchWaters()
        {
            if (water)
            {
                var waterRenderers = water.GetComponentsInChildren<MeshRenderer>().Where( mr => mr.name == "WaterMesh_0_0" ).ToList();
                water.meshRenderers.Clear();
                water.meshRenderers.AddRange( waterRenderers );
            }
        }

        internal void AdjustPixelError()
        {
            FetchExistingTerrain();
            if (tiles==null) return;
            tiles.ForEach( t =>
            {
                var terrain = t.GetComponent<Terrain>();
                if (terrain != null)
                {
                    terrain.heightmapPixelError = pixelError;
                }
            } );
        }

        internal void FixEdgesGround()
        {
            if (tiles==null) return;
            SetNeighbours();
            tiles.ForEach( t =>
            {
                var ter = t.GetComponent<Terrain>();
                var nb  = ter.leftNeighbor;
                if (nb!=null && ter.terrainData != null && nb.terrainData!=null)
                {
                    float[,] ttedge = ter.terrainData.GetHeights(0, 0, 1, heightMapResolution+1);
                    float[,] nbEdge = nb.terrainData.GetHeights(heightMapResolution, 0, 1, heightMapResolution+1);
                    for (int i = 0;i<heightMapResolution+1;i++)
                    {
                        var avg = (ttedge[i, 0] + nbEdge[i, 0]) * 0.5f;
                        ttedge[i, 0] = avg;
                    }
                    ter.terrainData.SetHeights( 0, 0, ttedge );
                    nb.terrainData.SetHeights( heightMapResolution, 0, ttedge );
                }
                nb = ter.bottomNeighbor;
                if (nb!=null && nb.terrainData!=null)
                {
                    float[,] ttedge = ter.terrainData.GetHeights(0, heightMapResolution, heightMapResolution+1, 1);
                    float[,] nbEdge = nb.terrainData.GetHeights(0, 0, heightMapResolution+1, 1 );
                    for (int i = 0;i<heightMapResolution+1;i++)
                    {
                        var avg = (ttedge[0, i] + nbEdge[0, i]) * 0.5f;
                        ttedge[0, i] = avg;
                    }
                    ter.terrainData.SetHeights( 0, heightMapResolution, ttedge );
                    nb.terrainData.SetHeights( 0, 0, ttedge );
                }
            } );
        }

        internal void FixEdgesWater()
        {
            if (tiles==null)
                return;
            SetNeighbours();
            int nTiles = Mathf.RoundToInt( (float)tileSize / waterTileSize );

            tiles.ForEach( t =>
            {
                var patchList = new List<(int, int)>();
                var waterMesh1 = t.transform.Find( "WaterMesh" );
                if (waterMesh1==null)
                    return;
                var v1 = waterMesh1.GetComponent<MeshFilter>().sharedMesh.vertices;
                if (v1.Length==0)
                    return;

                var ter = t.GetComponent<Terrain>();
                if (t.GetComponent<TerrainTile>()?.terrain.leftNeighbor!=null)
                {
                    var nb = t.GetComponent<TerrainTile>()?.terrain.leftNeighbor.GetComponent<TerrainTile>();
                    var waterMesh2 = t.GetComponent<TerrainTile>()?.terrain.leftNeighbor.transform.Find("WaterMesh");
                    if (waterMesh2!=null)
                    {
                        var v2 = waterMesh2.GetComponent<MeshFilter>().sharedMesh.vertices;
                        if (v2.Length!=0)
                        {
                            for (int i = 0;i < t.GetComponent<TerrainTile>()?.waterEdgeTilesKeys.Count;i++)
                            {
                                var nbWaterEdgeValues = nb.waterEdgeTilesValues;
                                if (t.GetComponent<TerrainTile>()?.waterEdgeTilesKeys[i].x == 0)
                                {
                                    int j = nb.waterEdgeTilesKeys.FindIndex( key =>
                                    {
                                        return key.x==nTiles-1 && key.y==t.GetComponent<TerrainTile>()?.waterEdgeTilesKeys[i].y;
                                    } );
                                    if (j >= 0)
                                    {
                                        int of1 = t.GetComponent<TerrainTile>().waterEdgeTilesValues[i];
                                        int of2 = nbWaterEdgeValues[j];
                                        patchList.Add( (of1, of2+1) );
                                        Debug.Assert( t.GetComponent<TerrainTile>()?.waterEdgeTilesKeys[i].x==0 && nb.waterEdgeTilesKeys[j].x==nTiles-1 && t.GetComponent<TerrainTile>()?.waterEdgeTilesKeys[i].y==nb.waterEdgeTilesKeys[j].y );
                                    }
                                }
                            }

                            if (patchList.Count > 0) // Align vertices elevation.
                            {
                                patchList.ForEach( patch => 
                                {
                                    var diff1 = waterMesh1.transform.TransformPoint(new Vector3(0, v1[patch.Item1].y, 0)).y -
                                                 waterMesh2.transform.TransformPoint(new Vector3(0, v2[patch.Item2].y, 0)).y;

                                    var diff2 = waterMesh1.transform.TransformPoint(new Vector3(0, v1[patch.Item1+2].y, 0)).y -
                                                 waterMesh2.transform.TransformPoint(new Vector3(0, v2[patch.Item2+2].y, 0)).y;

                                    v1[patch.Item1].y -= diff1;
                                    v1[patch.Item1+2].y -= diff2;
                                } );
                            }
                        }
                    }
                }

                // bottom
                if (t.GetComponent<TerrainTile>()?.terrain.bottomNeighbor != null)
                {
                    var nb = t.GetComponent<TerrainTile>()?.terrain.bottomNeighbor.GetComponent<TerrainTile>();
                    var waterMesh2 = t.GetComponent<TerrainTile>()?.terrain.bottomNeighbor.transform.Find("WaterMesh");
                    if (waterMesh2 != null)
                    {
                        var v2 = waterMesh2.GetComponent<MeshFilter>().sharedMesh.vertices;
                        if (v2.Length!=0)
                        {
                            patchList.Clear();
                            for (int i = 0;i < t.GetComponent<TerrainTile>()?.waterEdgeTilesKeys.Count;i++)
                            {
                                var nbWaterEdgeValues = nb.waterEdgeTilesValues;
                                if (t.GetComponent<TerrainTile>()?.waterEdgeTilesKeys[i].y == 0)
                                {
                                    int j = nb.waterEdgeTilesKeys.FindIndex( key => (key.y==nTiles-1) && (key.x==t.GetComponent<TerrainTile>()?.waterEdgeTilesKeys[i].x) );
                                    if (j >= 0)
                                    {
                                        int of1 = t.GetComponent<TerrainTile>().waterEdgeTilesValues[i];
                                        int of2 = nbWaterEdgeValues[j];
                                        patchList.Add( (of1, of2+2) );
                                        Debug.Assert( t.GetComponent<TerrainTile>()?.waterEdgeTilesKeys[i].y==0 && nb.waterEdgeTilesKeys[j].y==nTiles-1 && t.GetComponent<TerrainTile>()?.waterEdgeTilesKeys[i].x==nb.waterEdgeTilesKeys[j].x );
                                    }
                                }
                            }

                            if (patchList.Count > 0) // Align vertices elevation.
                            {
                                patchList.ForEach( patch =>
                                {
                                    var diff1 = waterMesh1.transform.TransformPoint( new Vector3( 0, v1[patch.Item1].y, 0 ) ).y -
                                                  waterMesh2.transform.TransformPoint( new Vector3( 0, v2[patch.Item2].y, 0 ) ).y;

                                    var diff2 = waterMesh1.transform.TransformPoint( new Vector3( 0, v1[patch.Item1+1].y, 0 ) ).y -
                                                  waterMesh2.transform.TransformPoint( new Vector3( 0, v2[patch.Item2+1].y, 0 ) ).y;
                                    
                                    v1[patch.Item1].y -= diff1;
                                    v1[patch.Item1+1].y -= diff2;
                                } );
                            }
                        }
                    }
                }

                // Store back aligned vertices.
                waterMesh1.GetComponent<MeshFilter>().sharedMesh.vertices = v1;
            } );
        }

        internal void RefreshMaterials()
        {
            if (tiles==null)
                return;
            tiles.ForEach( t => t.GetComponent<TerrainTile>()?.LinkLayersAndMaterial() );
        }

        void CheckPreliminaries()
        {
            numActiveVtiles   = 0;
            numVTilesFinished = 0;
            numStartedTiles   = 0;
            numFinishedTiles  = 0;

            tileSize = RDUtils.CalcTileSizeRD( zoom );
            RDUtils.GPS2RD( originWGS84.x, originWGS84.y, out originRDX, out originRDY );

            zoom = Mathf.Min(zoom, vTileZoom ); // It has no point to make a smaller tile than the min vector tile size (which is 12).
            numVtilesPerWide = (1 << (vTileZoom-zoom));
            vtileResolution  = controlResolution / numVtilesPerWide;
            numVtilesPerTile = numVtilesPerWide * numVtilesPerWide;

            if (controlResolution % vtileResolution != 0 )
            {
                throw new InvalidOperationException( "Invalid vtile resolution" );
            }

            if (controlResolution % 128 != 0)
            {
                throw new InvalidOperationException( "Control resolution must be dividable by 128" );
            }

            if (controlResolution % heightMapResolution != 0)
            {
                throw new InvalidOperationException( "Control resolution must be dividable by heigtMapResolution, e.g. 4096 and 128" );
            }

            VTileClippingVertices = new []
            {
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, 0.5),
            };

            float vTileSize = (float)RDUtils.CalcTileSizeRD( vTileZoom );
            for (int i = 0;i < VTileClippingVertices.Length;i++)
            {
                VTileClippingVertices[i] *= vTileSize;
                VTileClippingVertices[i] += new OpenTK.Mathematics.Vector3d( vTileSize/2, 0, vTileSize/2);
            }

            VTileClippingIndices = new [] {
                0,2,3,
                0,3,1,
                8,4,5,
                8,5,9,
                10,6,7,
                10,7,11,
                12,13,14,
                12,14,15,
                16,17,18,
                16,18,19,
                20,21,22,
                20,22,23,
            };
        }

        void CreateLayerNamesList()
        {
            // Validate this a bit (user input).
            if (layers == null || layers.Count == 0)
            {
                Debug.LogError( "No layers specified!" );
                return;
            }
            layerNamesList = new List<List<string>>();
            for (int i = 0;i< layers.Count;i++)
            {
                var l = layers[i];
                if ( l.matchingNames == null || l.matchingNames.Count == 0 )
                {
                    Debug.LogWarning( $"Layer {i} has no valid featureId specified, it will not match anything." );
                }
                else
                {
                    if(l.matchingNames.Any( id => string.IsNullOrWhiteSpace(id)))
                    {
                        Debug.LogWarning( $"Layer {i} has one or more invalid (empty) feature ids." );
                    }
                    layerNamesList.Add( l.matchingNames );
                }
            }
            var flattenNamesList = layerNamesList.SelectMany( k => k );
            int namesCount       = flattenNamesList.Count();
            int uniqueNamesCount = flattenNamesList.Distinct().Count();
            if ( namesCount != uniqueNamesCount )
            {
                Debug.LogWarning( $"Layers have overlapping matching names, this may result in mismatching layers." );
            }
        }

        Terrain GetTerrain( string prefix, int x, int y, int zoom )
        {
            var terrain = GameObject.Find( $"{prefix}_{x}_{y}_{zoom}" );
            if ( terrain != null )
            {
                return terrain.GetComponent<Terrain>();
            }
            return null;
        }

        void SetNeighbours()
        {
            FetchExistingTerrain();
            if (tiles == null || tiles.Count == 0)
                return;
            int zoomOfTheseTiles = int.Parse(tiles[0].transform.parent.name.Split( '_' )[1]);
            object anyTileNotNull = false;
            tiles.ForEach( t =>
            {
                int x = int.Parse(t.name.Split( '_' )[1]);
                int y = int.Parse(t.name.Split( '_' )[2]);
                var top    = GetTerrain( prefix, x, y+1, zoomOfTheseTiles );
                var bottom = GetTerrain( prefix, x, y-1, zoomOfTheseTiles );
                var left   = GetTerrain( prefix, x-1, y, zoomOfTheseTiles );
                var right  = GetTerrain( prefix, x+1, y, zoomOfTheseTiles );
                if (top!=null && bottom!=null || left!=null ||right!=null)
                {
                    anyTileNotNull=true;
                }
                t.GetComponent<Terrain>().SetNeighbors( left, top, right, bottom );
            } );
        }

        int BuildTile( Vector2Int centre, int x, int y )
        {
            var coord = centre + new Vector2Int( x, -y);
            if (coord.x < 0 || coord.y < 0 || coord.x >= (1<<zoom) || coord.y >= (1<<zoom))
            {
                // Skip outside bounds tiles.
                return 0;
            }
            var tileName = $"{prefix}_{coord.x}_{coord.y}_{zoom}";
            if (GameObject.Find( tileName )!=null)
                return 0; // Only build missing tiles.
            if (!overwriteExistingTiles)
            {
                var areaName = SceneManager.GetActiveScene().name;
                var tilePath = Path.Combine( Application.dataPath, $"{areaName}/tiles_{zoom}", $"tile_{coord.x}_{coord.y}").FS();
                if (Directory.Exists( tilePath ))
                {
                    var prefab = AssetDatabase.LoadAssetAtPath<GameObject>( tilePath.ToAssetsPath( true ) + "/tile.prefab" );
                    if (prefab != null)
                    {
                        var tileFromCache = Instantiate( prefab );
                        tileFromCache.name = $"{prefix}_{coord.x}_{coord.y}_{zoom}";
                        if (IsValid( tileFromCache ))
                            tiles.Add( tileFromCache );
                        else
                            Debug.LogWarning( tileFromCache.name + " is not valid." );
                        return 0;
                    }
                }
            }
            var goTile = new GameObject( $"{prefix}_{coord.x}_{coord.y}_{zoom}" );
            {
                var tile = goTile.AddComponent<TerrainTile>();
                tiles.Add( goTile );    
                tile.builder        = this;
                var vTileSize       = RDUtils.CalcTileSizeRD( 12 );
                tile.originRDX      = RDUtils.Tile2RD( coord, zoom ).rdX;
                tile.originRDY      = RDUtils.Tile2RD( coord, zoom ).rdY;
                tile.vTileOrigin    = RDUtils.RD2Tile( tile.originRDX + vTileSize/2, tile.originRDY + vTileSize/2, 12 );
                tile.tileOrigin     = coord;
                tile.terrainOffset  = new Vector2Int( x, y );
                return tile.Build();
            }
        }

        private bool IsValid(GameObject tile)
        {
            Terrain ter;
            return tile && (ter = tile.GetComponent<Terrain>()) && ter.terrainData;
        }

        IEnumerator YieldOnTooManyDownloads()
        {
            while (numActiveVtiles > maxDownloads)
            {
                if ( numActiveVtiles != numPrevActiveVTiles || numVTilesFinished != numPrevVTilesFinished)
                {
                    numPrevActiveVTiles    = numActiveVtiles;
                    numPrevVTilesFinished  = numVTilesFinished;
                    //Debug.Log( $"VTiles: Active: {numActiveVtiles} | Finished: {numVTilesFinished} | Finished Tiles: {numFinishedTiles}" );
                }
                yield return null;
            }
        }

        IEnumerator CreateTerrain()
        {
            var requestedCentreTile = RDUtils.RD2Tile( originRDX, originRDY, zoom );
            bool buildSingle = ( buildSpecific.x >= 0 && buildSpecific.y >= 0 && isGenerating );
            if ( buildSingle )
            {
                requestedCentreTile = buildSpecific;
            }
            GameObject terrainRoot = null;
            if (!isGenerating)
            {
                var requestedCentreTileRD = RDUtils.Tile2RD( requestedCentreTile, zoom );
                (double x, double y) offset = ( requestedCentreTileRD.rdX - originRDX, requestedCentreTileRD.rdY - originRDY ); // Offset is calcaulated from smallest, so that tiles align with bigger versions correctly.
                terrainRoot = FetchExistingTerrain();
                if (terrainRoot==null)
                {
                    terrainRoot = new GameObject( $"TerrainRoot_{zoom}" );
                    terrainRoot.transform.position = new Vector3( (float)offset.x, -terrainHeight/2, (float)offset.y );
                    terrainRoot.AddComponent<TileEnabler>();
                }
            }
            CreateWater( terrainRoot );
            AssetDatabase.DisallowAutoRefresh();
            totalNumVTiles = 0;
            totalNumVTiles += BuildTile( requestedCentreTile, 0, 0 );
            if (!buildSingle)
            {
                for (int c = 1;c <= numRings;c++)
                {
                    for (int y2 = -c;y2 <= c;y2++)
                    {
                        int numNewVTiles = BuildTile( requestedCentreTile, -c, y2 );
                        if (numNewVTiles > 0 )
                        {
                            yield return YieldOnTooManyDownloads();
                        }
                    }
                    for (int y2 = -c;y2 <= c;y2++)
                    {
                        int numNewVTiles = BuildTile( requestedCentreTile, c, y2 );
                        if (numNewVTiles > 0)
                        {
                            yield return YieldOnTooManyDownloads();
                        }
                    }
                    for (int x2 = -c+1;x2 <= c-1;x2++)
                    {
                        int numNewVTiles = BuildTile( requestedCentreTile, x2, c );
                        if (numNewVTiles > 0)
                        {
                            yield return YieldOnTooManyDownloads();
                        }
                    }
                    for (int x2 = -c+1;x2 <= c-1;x2++)
                    {
                        int numNewVTiles = BuildTile( requestedCentreTile, x2, -c );
                        if (numNewVTiles > 0)
                        {
                            yield return YieldOnTooManyDownloads();
                        }
                    }
                }
            } 
            if (!isGenerating)
            {
                tiles.RemoveAll( t => t == null );
                tiles.ForEach( t =>
                {
                    t?.transform.SetParent( terrainRoot.transform, false );
                } );
                SetNeighbours();
            }
            AssetDatabase.AllowAutoRefresh();
        }

        void CreateWater( GameObject terrainRoot )
        {
            if (terrainRoot == null)
                return; // When generating tiles, is null.

            water = terrainRoot.GetComponent<WaterSurface>();
            if (null==water)
            {
                water = terrainRoot.AddComponent<WaterSurface>();
                water.geometryType = WaterGeometryType.Custom;
                water.repetitionSize = 250;
                water.largeWindSpeed = 25;
                water.ripplesChaos = 0;
                water.ripplesWindSpeed = 11;
                water.largeChaos = 0.2f;
                water.largeBand0Multiplier = 0.512f;
                water.largeBand1Multiplier = 0.102f;
                water.timeMultiplier = 0.4f;
                water.maxRefractionDistance = 0.3f;
                water.absorptionDistance = 3;
                water.tessellation = false;
                water.deformation = false;
                water.caustics = false;
                water.underWater = false;
                water.smoothnessFadeStart = 100;
                water.smoothnessFadeDistance = 5000;
                water.startSmoothness = 0.8559129f;
                water.endSmoothness = 0.6907398f;
                water.ambientScattering = 0.136f;
                water.heightScattering = 0.132f;
                water.displacementScattering = 0.183f;
            }
        }

        GameObject FetchExistingTerrain()
        {
            tiles = new List<GameObject>();
            tiles.Clear();

            var terrainRoot = GameObject.Find( $"TerrainRoot_{zoom}" );
            if (terrainRoot == null)
                return null;
            
            for (int i = 0;i< terrainRoot.transform.childCount;i++)
            {
                var tile = terrainRoot.transform.GetChild( i );
                if (tile != null)
                {
                    tiles.Add( tile.gameObject );
                }
            }

            return terrainRoot;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.magenta;
            float tileSize = (float)RDUtils.CalcTileSizeRD(zoom);
            Gizmos.DrawWireCube( new Vector3( (float)offsetX, 0, (float)offsetY ) + new Vector3( tileSize, 0, tileSize )/2, new Vector3( (float)boundsSize, 100, (float) boundsSize ) );
        }
    }

#if UNITY_EDITOR
    [CustomEditor( typeof( TerrainBuilder ) )]
    [InitializeOnLoad]
    public class TerrainBuilderEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            TerrainBuilder builder = (TerrainBuilder)target;

            //EditorGUILayout.HelpBox( "Max detail is: Zoom 12.", MessageType.Info, true );
            EditorGUILayout.HelpBox( "Max detail is: Zoom 12.\nLook up a location in Google maps and copy GPS coordinates in Origin WGS84 X, Y" +
                "\nIf you have forgotten a previously entered GPS coord, they don't need to match exactly. AreaName MUST match.", MessageType.Info, true );
            EditorGUILayout.HelpBox( "Press CTRL-S when done building terrain, otherwise result may appears wrong.", MessageType.Warning, true );
            

            if (builder.groundLayer.value == 0)
            {
                EditorGUILayout.HelpBox( "Ground layer cannot be 'Nothing' when 'Set On Ground' is turned on.", MessageType.Warning );
            }

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Build" ))
                {
                    builder.issuedSyncCoords = true;
                    builder.issuedBuild = true;
                }
                if (GUILayout.Button( "Sync coords" ))
                {
                    builder.issuedSyncCoords = true;
                }
                if (GUILayout.Button( "Fetch waters" ))
                {
                    builder.FetchWaters();
                }
                if (GUILayout.Button( "Fix edges" ))
                {
                    builder.FixEdgesGround();
          //         builder.FixEdgesWater();
                }
                if (GUILayout.Button( "Adjust pix error" ))
                {
                    builder.AdjustPixelError();
                }
                if (GUILayout.Button( "Stop" ))
                {
                    builder.Cancel();
                }
            }
            GUILayout.EndHorizontal();

            DrawDefaultInspector();
        }
    }

#endif
}

#endif