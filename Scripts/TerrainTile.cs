using Mapbox.Vector.Tile;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;
using System.Threading;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.SceneManagement;





#if UNITY_EDITOR

using UnityEditor;

namespace Wander
{
    class TileRequest
    {
        internal bool started;
        internal bool postManaged;
        internal int deltaTileX; // Relative tile index with respect to origin up left corner. So, 0, -1, etc.
        internal int deltaTileY;
        internal VectorTile vtile;
        internal byte[] controlData;
        internal Task processVtileTask;
    }

    [ExecuteAlways()]
    public class TerrainTile : MonoBehaviour
    {
        enum State
        {
            None,
            Processing,
            Finished
        }

        State state = State.None;

        [ReadOnly] public double originRDX;
        [ReadOnly] public double originRDY;
        [ReadOnly] public Vector2Int tileOrigin;    // Based on requested zoom.
        [ReadOnly] public Vector2Int vTileOrigin;   // Based on zoomlevel 12 (only available vTile).
        [ReadOnly] public TerrainBuilder builder;
        [ReadOnly] public TerrainData terrainData;
        [ReadOnly] public Terrain terrain;
        [ReadOnly] public Vector2Int terrainOffset;
        [ReadOnly] public Material terrainMat;
        [ReadOnly] public List<Vector2Int> waterEdgeTilesKeys;
        [ReadOnly] public List<int> waterEdgeTilesValues;
        [ReadOnly] public byte [] remappedLayerIndicesArray;
        [ReadOnly] public int numVTilesFinished;

        private List<TileRequest> vtileRequests;
        private byte[] controlData;
        private AsyncHeightData asyncHeightHandle;
        private Task combineVTilesTask;
        private Task<float[,]> normalizeHeightTask;
        private Task adjustWaterHeightTask;
        private Task<List<(List<Vector3>, List<int>, Vector2Int)>> createWaterTilesTask;
        private Task<List<(Vector3, GameObject)>> createFoliageTask;
        private List<Vector3> dbgWaterLowestEdgePoints;
        
        private bool cancelToken = false;
        private Dictionary<Vector2Int, int> waterEdgeTiles;
        

        internal async void CancelAndClean()
        {
            cancelToken = true;
            if ( normalizeHeightTask != null )
            {
                await normalizeHeightTask;
                normalizeHeightTask = null;
            }    
            if (vtileRequests != null)
            {
                vtileRequests.ForEach( async r =>
                {
                    if (r.processVtileTask!=null)
                    {
                        await r.processVtileTask;
                    }
                } );
            }
            if (combineVTilesTask!=null)
            {
                await combineVTilesTask;
            }
            if (adjustWaterHeightTask != null)
            {
                await adjustWaterHeightTask;
            }
            if (createWaterTilesTask != null)
            {
                await createWaterTilesTask;
            }
            numVTilesFinished = 0;
            combineVTilesTask = null;
            createWaterTilesTask = null;
            adjustWaterHeightTask = null;
            normalizeHeightTask = null;
            vtileRequests = null;
            controlData = null;
            Thread.MemoryBarrier();
            state = State.Finished;
            builder.numFinishedTiles += 1;
        }

        internal int Build()
        {
            builder.numStartedTiles++;
            //Debug.Log( $"Starting tile {builder.numStartedTiles} origin: {tileOrigin.x} {tileOrigin.y}" );
            //CancelAndClean();
            dbgWaterLowestEdgePoints = new List<Vector3>();
            controlData = new byte[builder.controlResolution*builder.controlResolution/2]; // 4 bits per pixel, byte represents 2 pixels.
            state = State.Processing;
            cancelToken = false;
            DownloadVTiles();
            DownloadHeight();
            CreateTerrainData();
            Validation();
            return vtileRequests.Count;
        }

        void Update()
        {
            switch (state)
            {
                case State.None:
                    break;

                    /* Instead of sequentially going from one task to another, execute all at once and keep checking. */
                case State.Processing:

                    // See if terrain is done.
                    CheckTerrainRequests();
                    if ( vtileRequests == null ) // Can happen if compiling while executing these things in the background.
                    {
                        CancelAndClean();
                        return;
                    }
                    numVTilesFinished = vtileRequests.Count( r => (r.processVtileTask!=null && r.processVtileTask.IsCompleted) );
                    bool terrainDone  = (numVTilesFinished==vtileRequests.Count);

                    // See if height is done if requested.
                    if ( builder.loadHeight )
                    {
                        if (normalizeHeightTask == null && asyncHeightHandle != null && asyncHeightHandle.IsFinished() )
                        {
                            normalizeHeightTask = Task.Run( () =>
                            {
                                return CreateHeightMap( asyncHeightHandle.HeightData );
                            } );
                        }
                    }
                    bool heightDone = !builder.loadHeight || (normalizeHeightTask != null && normalizeHeightTask.IsCompleted);

                    if ( terrainDone && heightDone && combineVTilesTask==null)
                    {
                        combineVTilesTask = Task.Run( () => CombineVectorTilesAndFindUniqueLayers() );
                    }
                    bool vTileCombineDone = combineVTilesTask != null && combineVTilesTask.IsCompleted;

                    // Base water on current height (before water elevation is applied).
                    if (heightDone && vTileCombineDone && createWaterTilesTask == null && builder.WaterType != WaterType.None)
                    {
                        createWaterTilesTask = Task.Run( () =>
                        {
                            var res = builder.WaterType == WaterType.Islands ? 
                                CreateWaterIslands( normalizeHeightTask.Result, dbgWaterLowestEdgePoints ) :
                                CreateWaterTiles( normalizeHeightTask.Result, dbgWaterLowestEdgePoints, out waterEdgeTiles );
                            return res;
                        } );
                    }
                    bool waterTilesDone = builder.WaterType == WaterType.None || (createWaterTilesTask != null && createWaterTilesTask.IsCompleted);

                    if (waterTilesDone && vTileCombineDone && builder.applyWaterElevation)
                    {
                        if (adjustWaterHeightTask == null)
                        {
                            float [,] heightData = builder.loadHeight ? normalizeHeightTask.Result : null;
                            adjustWaterHeightTask = Task.Run( () => ApplyWaterElevation( heightData ) );
                        }
                    }
                    bool waterElevationDone = !builder.applyWaterElevation || (adjustWaterHeightTask != null && adjustWaterHeightTask.IsCompleted);

                    // Foliage must wait on terrain.
                    if (vTileCombineDone && createFoliageTask == null && builder.createFoliage)
                    {
                        Vector3 tilePosition = transform.position;
                        createFoliageTask = Task.Run( ()=> CreateFoliage(tilePosition ) );
                    }
                    bool foliageDone = !builder.createFoliage || (createFoliageTask != null && createFoliageTask.IsCompleted);

                    // Check if we are done.
                    if ( terrainDone && heightDone && vTileCombineDone && waterElevationDone && waterTilesDone && foliageDone )
                    {
                        DoFinalSteps();
                    }
                    break;
            }

            if (builder.autoRefreshMaterials && UnityEngine.Random.Range( 0, 15 )==0)
            {
                LinkLayersAndMaterial();
            }

            if (Application.isPlaying)
                enabled = false;
        }

        public void LinkLayersAndMaterial()
        {
            if (builder.layers == null || builder.layers.Count == 0)
            { 
                return;
            }

            if ( remappedLayerIndicesArray == null || remappedLayerIndicesArray.Length == 0 )
            {
                return;
            }

            terrainMat = new Material( remappedLayerIndicesArray.Length switch
            {
                <= 4 => builder.terrainMat4,
                <= 8 => builder.terrainMat8,
                <= 10 => builder.terrainMat10,
                _ => builder.terrainMat10
            } );
            terrainMat.SetTexture( "ControlTexture", terrain.materialTemplate.GetTexture( "ControlTexture" ) );

            for ( byte i = 0; i<  Mathf.Min( remappedLayerIndicesArray.Length, 10 ); i++ )
            {   
                byte terrainLayerIdx = remappedLayerIndicesArray[i];
                byte sequencedIdx    = i;
                if (terrainLayerIdx >= builder.layers.Count )
                {
                    continue;
                }
                var diffuseTex = builder.layers[terrainLayerIdx].mainTexture;
                var normalTex  = builder.layers[terrainLayerIdx].normalMap;
                var metallic   = builder.layers[terrainLayerIdx].metallic;
                var smoothness = builder.layers[terrainLayerIdx].smoothness;
                terrainMat.SetTexture( "Albedo" + sequencedIdx, diffuseTex );
                terrainMat.SetTexture( $"_Normal{sequencedIdx}_", normalTex );
                terrainMat.SetFloat( $"_Metallic_{sequencedIdx}", metallic );
                terrainMat.SetFloat( $"_Smoothness_{sequencedIdx}", smoothness );
                terrainMat.SetFloat( "_Tiling_" + sequencedIdx, builder.layers[terrainLayerIdx].tiling );
                var micro1 = builder.layers[terrainLayerIdx].microTexture1 !=null ? builder.layers[terrainLayerIdx].microTexture1 : diffuseTex;
                var micro2 = builder.layers[terrainLayerIdx].microTexture2 != null ? builder.layers[terrainLayerIdx].microTexture2 : diffuseTex;
                terrainMat.SetTexture( "Albedo" + sequencedIdx + "_1", micro1 );
                terrainMat.SetTexture( "Albedo" + sequencedIdx + "_2", micro2 );
            }

            terrain.materialTemplate = terrainMat;
        }

        void CreateTerrainData()
        {
            float tileSize = (float)builder.tileSize;

            terrain = gameObject.AddComponent<Terrain>();
            terrainData = new TerrainData();
            terrain.heightmapPixelError = builder.pixelError;

            terrain.terrainData       = terrainData;
            terrain.materialTemplate  = new Material( builder.terrainMat4 );
      //      terrainData.terrainLayers = builder.layers.Select( l => l.layer ).ToArray();
            terrainData.heightmapResolution = builder.heightMapResolution;
            terrainData.alphamapResolution  = 16; // Cannot put lower than this by Unity. Deleting wont work, Unity regenerates these maps on Save.
            terrainData.baseMapResolution   = 16;
            terrainData.size = new Vector3( tileSize, builder.terrainHeight, tileSize );

            gameObject.AddComponent<TerrainCollider>().terrainData = terrainData;
            gameObject.transform.localPosition = new Vector3( terrainOffset.x*(float)builder.tileSize, 0, terrainOffset.y*(float)builder.tileSize );
        }

        void Validation()
        {
            if (remappedLayerIndicesArray != null && remappedLayerIndicesArray.Length > 10)
            {
                Debug.LogWarning( name + " terrainLayerIdx is higher than layers specified in inspector." );
            }
        }

        string GetPDOKVectorTileUrl( int tileX, int tileY, int zoom )
        {
            return $"https://api.pdok.nl/lv/bgt/ogc/v1_0/tiles/NetherlandsRDNewQuad/{zoom}/{tileY}/{tileX}?f=mvt";
        }

        // This filter is specific to PDOK serving vector tiles. Other providers, will demand a different filtering schema.
        int PDOKLayerFilter( VectorTileLayer layer, List<KeyValuePair<string, object>> attribs )
        {
            var searchKeys = new [] { "functie", "fysiek_voorkomen", "type"};

            // Try 'functie' first, then 'fysiek_voorkomen', 'type' is needed for water.
            for (int m = 0;m < searchKeys.Length;m++)
            {
                for (int a = 0;a < attribs.Count;a++)
                {
                    if (attribs[a].Key != searchKeys[m])
                        continue;

                    string function = attribs[a].Value as string;

                    // Prefer exact match over partial match
                    for (int layerIdx = 0;layerIdx < builder.layerNamesList.Count;layerIdx++)
                    {
                        for (int layerNameIdx = 0;layerNameIdx < builder.layerNamesList[layerIdx].Count;layerNameIdx++)
                        {
                            if (function ==  builder.layerNamesList[layerIdx][layerNameIdx] )
                            {
                                return layerIdx;
                            }
                        }
                    }

                    // Check partial match
                    for (int layerIdx = 0;layerIdx < builder.layerNamesList.Count;layerIdx++)
                    {
                        for (int layerNameIdx = 0;layerNameIdx < builder.layerNamesList[layerIdx].Count;layerNameIdx++)
                        {
                            if (function.Contains( builder.layerNamesList[layerIdx][layerNameIdx] ))
                            {
                                return layerIdx;
                            }
                        }
                    }

                    break;
                }
            }

            // Try layer name.
            for (int layerIdx = 0;layerIdx < builder.layerNamesList.Count;layerIdx++)
            {
                for (int layerNameIdx = 0;layerNameIdx < builder.layerNamesList[layerIdx].Count;layerNameIdx++)
                {
                    if (layer.Name == builder.layerNamesList[layerIdx][layerNameIdx] )
                    {
                        return layerIdx;
                    }
                }
            }

            // Not found.
            return -1;
        }

        int PDOKDetermineRelativeHeight( VectorTileLayer layer, List<KeyValuePair<string, object>> attribs )
        {
            for ( int i = 0; i < attribs.Count; i++ ) 
            {
                var at = attribs[i];
                if ( at.Key == "relatieve_hoogteligging")
                {
                    return Mathf.FloorToInt( (float)(double)at.Value + 0.5f );
                }
            }
            return 0;
        }

        // This function is executed in a Task.Run().
        void ProcessVectorTile( TileRequest tile )
        {
            try
            {
                var remappedLayerIndices = new Dictionary<byte, byte>();
                tile.vtile.IdentifyLayers( PDOKLayerFilter, PDOKDetermineRelativeHeight );
                tile.vtile.Triangulate();
                if (builder.renderType == RenderType.Raytrace)
                {
                    tile.vtile.OptimizeForPointIsInsideTriangle( true );
                    tile.vtile.GenerateQuadTreeForRaytrace();
                    tile.controlData = tile.vtile.RaytraceToTexture( builder.vtileResolution, out _, ref cancelToken );
                }
                else
                {
                    tile.vtile.OptimizeForPointIsInsideTriangle( false );
                    tile.controlData = tile.vtile.RasterizeToTexture( builder.vtileResolution, out _, ref cancelToken );
                }
            //    tile.vtile = null; // DO this so GC can reclaim mem.
            //    GC.Collect();
            } catch (Exception ex ) { Debug.LogException( ex ); }   
        }

        void CombineVectorTilesAndFindUniqueLayers()
        {
            try
            {
                controlData = new byte[builder.controlResolution*builder.controlResolution/2];
                for (int i = 0;i < vtileRequests.Count;i++)
                {
                    var tile = vtileRequests[i];
                    var ofsx = tile.deltaTileX * builder.vtileResolution;
                    var ofsy = tile.deltaTileY * builder.vtileResolution;
                    var start = ofsy * builder.controlResolution/2 + ofsx/2;
                    for (int y = 0;y < builder.vtileResolution;y++)
                    {
                        Buffer.BlockCopy( tile.controlData, y * builder.vtileResolution/2, controlData, start + y*builder.controlResolution/2, builder.vtileResolution/2 );
                    }
                    Debug.Assert( tile.processVtileTask.IsCompleted );
                }

                var remappedLayerIndices = VectorTile.FindLayersInTexture( controlData, builder.controlResolution, ref cancelToken );
                remappedLayerIndicesArray = new byte[remappedLayerIndices.Count];
                foreach (var kvp in remappedLayerIndices)
                {
                    remappedLayerIndicesArray[kvp.Value] = kvp.Key;
                }
            } catch (Exception e) { Debug.LogException( e ); }  
        }

        void DownloadVTiles()
        {
            vtileRequests   = new List<TileRequest>();
            for (int y = 0; y < builder.numVtilesPerWide; y++ )
            {
                for (int x = 0; x <  builder.numVtilesPerWide; x++ )
                {
                    TileRequest td = new TileRequest();
                    td.deltaTileX = x;
                    td.deltaTileY = y;
                    string url = GetPDOKVectorTileUrl(vTileOrigin.x + x , vTileOrigin.y - y, 12);
                    td.vtile   = VectorTileLoader.LoadFromUrl( url, false );
                    vtileRequests.Add( td );
                }
            }
        }

        void CheckTerrainRequests()
        {
            if (vtileRequests==null)
                return;

            int numActive = 0;
            for (int i = 0;i < vtileRequests.Count;i++)
            {
                var tile  = vtileRequests[i];
                var vtile = tile.vtile;
                if (!tile.started)
                {
                    tile.started = true;
                    vtile.StartDownload();
                }
                if (vtile == null || vtile.IsFinished())
                {
                    if (!tile.postManaged)
                    {
                        tile.postManaged=true;
                        builder.numActiveVtiles -= 1;
                        builder.numVTilesFinished += 1;
                        //Debug.Assert( builder.numActiveVtiles >= 0 );
                        if (!vtile.Valid)
                        {
                            vtileRequests.RemoveAt( i );
                            i--;
                        }
                        else
                        {
                            tile.processVtileTask = Task.Run( () => ProcessVectorTile( tile ) );
                            i--;
                        }
                    }
                }
                else
                {
                    numActive++;
                }
                if (numActive >= builder.maxDownloads)
                    break;
            }
        }

        void DownloadHeight()
        {
            if (!builder.loadHeight)
                return;
            var url = GeoTiffHeight.BuildPDOKWCSUrl( originRDX, originRDX+builder.tileSize, originRDY, originRDY+builder.tileSize, builder.heightMapResolution, builder.heightMapResolution );
            asyncHeightHandle = GeoTiffHeight.LoadFromUrl( url, builder.filterHeight, 5, 0  );
        }

        Texture2D CreateControlTexture()
        {
            if (terrain == null)
                return null; // Can be deleted in scene.
            var controlTexture = new Texture2D( builder.controlResolution/4, builder.controlResolution, TextureFormat.RGBA4444, false );
            terrain.materialTemplate.SetTexture( "ControlTexture", controlTexture );
            controlTexture.SetPixelData( controlData, 0, 0 );
            controlTexture.Apply( false, false );
            return controlTexture;
        }

        // This is function is called in Task.Run().
        float[,] CreateHeightMap( HeightData heightData )
        {
            try
            {
                var heights = new float[builder.heightMapResolution+1, builder.heightMapResolution+1];
                int width   = builder.heightMapResolution;
                int height  = builder.heightMapResolution;
                for (int y = 0;y<height;y++)
                {
                    if (state== State.None)
                        return null; // Cancelling.

                    for (int x = 0;x<width;x++)
                    {
                        if (heightData.data != null)
                        {
                            float absHeight = heightData.data[y*width+x];
                            float normalizedHeight = (absHeight+builder.terrainHeight/2) / builder.terrainHeight;
                            heights[height-y-1, x] = normalizedHeight;
                        }
                        else
                        {
                            heights[height-y-1, x] = 0.5f;
                        }
                    }
                }
                for (int y = 0;y <height;y++)
                {
                    heights[y, width] = heights[y, width-1];
                }
                for (int x = 0;x <heightData.width;x++)
                {
                    heights[height, x] = heights[height-1, x];
                }
                heights[height, width] = (heights[height, width-1] + heights[height-1, width]) / 2;
                return heights;
            }
            catch (Exception e)
            {
                Debug.LogException( e );
            }
            return default;
        }

        void UpdateWaterIslandEdge(Dictionary<(int e1, int e2), (int, Vector3)> edgeList, int e1, int e2, Vector3 centre)
        {
            if (edgeList.TryGetValue( (e1, e2), out _ ))
            {
                edgeList[(e1, e2)] = (2, centre);
            }
            else if (edgeList.TryGetValue( (e2, e1), out _ ))
            {
                edgeList[(e2, e1)] = (2, centre);
            }
            else
            {
                edgeList[(e2, e1)] = (1, centre);
            }
        }

        // This is function is called in Task.Run().
        List<(List<Vector3>, List<int>, Vector2Int)> CreateWaterIslands( float[,] heights, List<Vector3> dbgDebugLowestWaterHeightsOut )
        {
            Debug.Assert( builder.WaterType == WaterType.Islands );
            var waterIslands    = new List<(List<Vector3>, List<int>, Vector2Int)>();
            float tileSizeVTile = (float) RDUtils.CalcTileSizeRD( builder.vTileZoom );
            float bigTileSize   = (float) RDUtils.CalcTileSizeRD( builder.zoom );
            float normalizedTileSize = tileSizeVTile / bigTileSize;
            byte waterPixelIndx = GetRemappedLayerIdx( builder.waterLayerIdx );
            for (int i = 0; i < vtileRequests.Count; i++)
            {
                var req  = vtileRequests[i];
                float ofx = req.deltaTileX * normalizedTileSize;
                float ofy = req.deltaTileY * normalizedTileSize;
                var waters = req.vtile.GetWaterPolygons( builder.waterLayerIdx, ref cancelToken );
                for (int j = 0; j < waters.Count; j++)
                {
                    for (int k =0; k<waters[j].Count; k++)
                    {
                        var projectedVertices = waters[j][k].vertices.Select( v =>
                        {
                            var o = new Vector3(v.x, 0, 4096-v.y);
                            o *= (1.0f/4096) * tileSizeVTile;
                            return o;
                        } ).ToList();

                        var indices = new int[projectedVertices.Count];
                        for (int q = 0;q < projectedVertices.Count;q++)
                        {
                            indices[q] = q;
                        }

                        try
                        {
                            var solid = CSG.BooleanOperationCore( builder.VTileClippingVertices, projectedVertices.Select( pv => new OpenTK.Mathematics.Vector3d(pv.x, pv.y, pv.z)).ToArray(), builder.VTileClippingIndices, indices, CSG.Boolean.Intersection);
                            if (!solid.IsEmpty)
                            {
                                var edgeList   = new Dictionary<(int e1, int e2), (int numTris, Vector3 centre)>();
                                var csgVertics = solid.GetVertices().Select( v => new Vector3( (float)v.X, (float)v.Y, (float)v.Z ) ).ToList();
                                var csgIndices = solid.GetIndices().ToList();
                                // Determine avg height of water surface.
                                double avgWaterHeight = 0;
                                int numSamples = 0;
                                Vector3 lowestHeight = new Vector3(0, float.MaxValue, 0);
                                if (heights != null)
                                {
                                    for (int l = 0;l < csgIndices.Count;l+=3)
                                    {
                                        var v0 = csgIndices[l+0];
                                        var v1 = csgIndices[l+1];
                                        var v2 = csgIndices[l+2];
                                        var vt0 = csgVertics[v0];
                                        var vt1 = csgVertics[v1];
                                        var vt2 = csgVertics[v2];
                                        var cen = (vt0+vt1+vt2)/3;
                                        UpdateWaterIslandEdge( edgeList, v0, v1, cen );
                                        UpdateWaterIslandEdge( edgeList, v0, v2, cen );
                                        UpdateWaterIslandEdge( edgeList, v1, v2, cen );
                                        cen *= (normalizedTileSize / tileSizeVTile); // normalized space
                                        float fx2 = cen.x + ofx;
                                        float fy2 = cen.z + ofy;
                                        int x10 = Mathf.FloorToInt(fx2 *builder.controlResolution);
                                        int y10 = Mathf.FloorToInt(fy2 *builder.controlResolution);
                                        byte newPixel = GetPixel(x10, y10);
                                        if (newPixel == waterPixelIndx)
                                        {
                                            int x4 = Mathf.FloorToInt( fx2*builder.heightMapResolution );
                                            int y4 = Mathf.FloorToInt( fy2*builder.heightMapResolution );
                                            float surfaceHeight = heights[y4, x4] * builder.terrainHeight;// - builder.terrainHeight/2;
                                            avgWaterHeight += surfaceHeight;
                                            numSamples++;
                                            if (surfaceHeight < lowestHeight.y)
                                            {
                                                lowestHeight = new Vector3( fx2 * bigTileSize, surfaceHeight, fy2 * bigTileSize );
                                            }
                                        }
                                    }
                                }
                                if (numSamples > 0)
                                {
                                    avgWaterHeight = avgWaterHeight/(numSamples) + builder.waterHeightOffset;
                                }
                                else
                                {
                                    avgWaterHeight = builder.waterHeightOffset * 0.66f;
                                }
                                if (lowestHeight.y != float.MaxValue && dbgDebugLowestWaterHeightsOut != null )
                                {
                                    dbgDebugLowestWaterHeightsOut.Add( new Vector3( lowestHeight.x, (float)avgWaterHeight, lowestHeight.z ) );
                                }
                                var edges = edgeList.Where( kvp => kvp.Value.numTris==1 ).Select( kvp => (kvp.Key, kvp.Value.centre) ).ToList();
                                var newEdgeVertices = new List<Vector3>();
                                var newEdgeIndices  = new List<int>();
                                for (int q = 0; q < edges.Count; q++)
                                {
                                    var v0  = csgVertics[edges[q].Key.e1];
                                    var v1  = csgVertics[edges[q].Key.e2];
                                    var cen = edges[q].centre;
                                    var ve  = (v1-v0).normalized;
                                    var ce  = cen - v0;
                                    var len = Vector3.Dot(ce, ve );
                                    var n0  = (v0+ve*len - cen).normalized;
                                    //var n0 = new Vector3( -(v1.y-v0.y), 0, v1.x-v0.x ).normalized;
                                    var v2 = v0 + (new Vector3(0, -0.55f, 0 ) + n0)*builder.waterIslandCapScale;
                                    var v3 = v1 + (new Vector3(0, -0.55f, 0 ) + n0)*builder.waterIslandCapScale;
                                    newEdgeVertices.Add( v2 );
                                    newEdgeVertices.Add( v3 );
                                    if (Vector3.Cross( ve, ce ).y < 0)
                                    {
                                        // t1
                                        newEdgeIndices.Add( edges[q].Key.e1 );
                                        newEdgeIndices.Add( edges[q].Key.e2 );
                                        newEdgeIndices.Add( newEdgeVertices.Count-2 + csgVertics.Count );
                                        // t2
                                        newEdgeIndices.Add( edges[q].Key.e2 );
                                        newEdgeIndices.Add( newEdgeVertices.Count-1 + csgVertics.Count );
                                        newEdgeIndices.Add( newEdgeVertices.Count-2 + csgVertics.Count );
                                    }
                                    else
                                    {
                                        // t1
                                        newEdgeIndices.Add( newEdgeVertices.Count-2 + csgVertics.Count  );
                                        newEdgeIndices.Add( edges[q].Key.e2 );
                                        newEdgeIndices.Add( edges[q].Key.e1 );
                                        // t2
                                        newEdgeIndices.Add( newEdgeVertices.Count-2 + csgVertics.Count );
                                        newEdgeIndices.Add( newEdgeVertices.Count-1 + csgVertics.Count );
                                        newEdgeIndices.Add( edges[q].Key.e2 );
                                    }
                                }
                                csgVertics.AddRange( newEdgeVertices );
                                csgIndices.AddRange( newEdgeIndices );
                                for (int q = 0;q < csgVertics.Count;q++)
                                {
                                    csgVertics[q] += new Vector3( tileSizeVTile * req.deltaTileX, (float)avgWaterHeight, tileSizeVTile*req.deltaTileY );
                                }
                                waterIslands.Add( (csgVertics, csgIndices, new Vector2Int(req.deltaTileX, req.deltaTileY)) );
                            }
                        }
                        catch (Exception ex) { Debug.LogException( ex ); }
                    }
                }
            }
            return waterIslands;
        }

        // This is function is called in Task.Run().
        List<(List<Vector3>, List<int>, Vector2Int)> CreateWaterTiles( float[,] heights, List<Vector3> dbgLowestEdgePointsOut, out Dictionary<Vector2Int, int> edgeTiles )
        {
            var waterIslands = new List<(List<Vector3>, List<int>, Vector2Int)>();
            edgeTiles = new Dictionary<Vector2Int, int>();
            try
            {
                Debug.Assert( builder.WaterType == WaterType.Tiles );
                if (heights == null) // Possible if height from GeoTiff is not applied.
                {
                    heights = new float[builder.heightMapResolution+1, builder.heightMapResolution+1];
                }
                if (remappedLayerIndicesArray == null || !ContainsLayer(builder.waterLayerIdx))
                {
                    return default; // Exception in render vector tile or it has no water.
                }
                byte waterPixelIndx = GetRemappedLayerIdx( builder.waterLayerIdx );
                int nTiles          = Mathf.RoundToInt( (float)builder.tileSize / builder.waterTileSize );
                float tileSize      = (float)builder.tileSize / nTiles;
                int controlMapRes   = builder.controlResolution;
                float heightMapRes  = builder.heightMapResolution+1;
                bool [] visisted    = new bool[nTiles*nTiles];
                List<Vector3> vertices = new List<Vector3>();
                List<int > tileIndices = new List<int>();
                for (int y = 0;y < nTiles && !cancelToken;y++)
                {
                    for (int x = 0;x < nTiles && !cancelToken;x++)
                    {
                        var stack = new List<(int x, int y)>
                        {
                            (x, y),
                            (x-1, y),
                            (x+1, y),
                            (x, y+1),
                            (x, y-1)
                        };
                        //    float lowestHeight = float.MaxValue;
                        float lowestSurfaceHeight  = float.MaxValue;
                        double avgSurfaceWaterHeight = 0;
                        double avgSurfLandHeight = 0;
                        int numLandSurfaceSamples = 0;
                        int numWaterSurfaceSamples = 0;
                        Vector3 lowestSurfacePoint = Vector3.zero;
                        var newVertices = new List<Vector3>();
                        var newIndices  = new List<int>();
                        while (stack.Count > 0)
                        {
                            var coord = stack[0];
                            stack.RemoveAt( 0 );
                            if (!(coord.x >= 0 && coord.x < nTiles && coord.y >= 0 && coord.y < nTiles))
                                continue;

                            if (visisted[coord.y*nTiles + coord.x])
                                continue;

                            visisted[coord.y*nTiles + coord.x] = true;

                            double avgWaterLocalHeight = 0;
                            double avgLandLocalHeight = 0;
                            int numLandLocalSamples = 0;
                            int numWaterLocalSamples = 0;

                            int c = 2;
                            for (int y2 = -c;y2 <= c ;y2++)
                            {
                                int y3 = coord.y + y2;
                                for (int x2 = -c;x2 <= c ;x2++)
                                {
                                    int x3 = coord.x + x2;
                                    if (!(x3 >= 0 && x3 < nTiles && y3 >=0 && y3 < nTiles))
                                        continue;

                                    float fy2 = (float)y3 / nTiles;
                                    float fx2 = (float)x3 / nTiles;

                                    int x10 = Mathf.FloorToInt(fx2 *controlMapRes);
                                    int y10 = Mathf.FloorToInt(fy2 *controlMapRes);

                                    byte newPixel = GetPixel(x10, y10);
                                    int x4 = Mathf.FloorToInt( fx2*heightMapRes );
                                    int y4 = Mathf.FloorToInt( fy2*heightMapRes );
                                    float surfaceHeight = heights[y4, x4] * builder.terrainHeight - builder.terrainHeight/2;

                                    if (waterPixelIndx == newPixel)
                                    {
                                        avgWaterLocalHeight += surfaceHeight;
                                        avgSurfaceWaterHeight += surfaceHeight;
                                        numWaterLocalSamples++;
                                        numWaterSurfaceSamples++;
                                        if (surfaceHeight < lowestSurfaceHeight)
                                        {
                                            lowestSurfaceHeight = surfaceHeight;
                                            lowestSurfacePoint  = new Vector3( fx2 * (float)builder.tileSize, surfaceHeight, fy2 * (float)builder.tileSize );
                                        }
                                    }
                                    else
                                    {
                                        avgLandLocalHeight += surfaceHeight;
                                        avgSurfLandHeight += surfaceHeight;
                                        numLandSurfaceSamples++;
                                        numLandLocalSamples++;
                                    }
                                }
                            }

                            // If found water is true, there is always a valid surface sample. So it is safe to start building water tiles.
                            if (numWaterLocalSamples==0)
                            {
                                continue;
                            }

                            avgWaterLocalHeight /= numWaterLocalSamples;
                            if (numLandLocalSamples == 0 || avgWaterLocalHeight < avgLandLocalHeight/numLandLocalSamples - builder.waterHeightOffset)
                            {
                                Vector3 v0 = new Vector3( coord.x*tileSize, 0, coord.y*tileSize );
                                Vector3 v1 = new Vector3( (coord.x+1)*tileSize, 0, coord.y*tileSize );
                                Vector3 v2 = new Vector3( coord.x*tileSize, 0, (coord.y+1)*tileSize );
                                Vector3 v3 = new Vector3( (coord.x+1)*tileSize, 0, (coord.y+1)*tileSize );

                                newVertices.AddRange( new[] { v0, v1, v2, v3 } );
                                int of = vertices.Count + newVertices.Count-4;
                                newIndices.AddRange( new[] { of, of+2, of+1, of+1, of+2, of+3 } ); // t1 & t2

                                if (coord.x == 0 || coord.x == nTiles-1 || coord.y == 0 || coord.y == nTiles-1)
                                {
                                    edgeTiles.Add( new Vector2Int( coord.x, coord.y ), of );
                                }

                                stack.Add( (coord.x-1, coord.y) );
                                stack.Add( (coord.x+1, coord.y) );
                                stack.Add( (coord.x, coord.y-1) );
                                stack.Add( (coord.x, coord.y+1) );
                            }
                        }

                        // Set vertices to lowest elevation in water island.
                        avgSurfaceWaterHeight /= numWaterSurfaceSamples;
                        if (numLandSurfaceSamples ==0 || avgSurfaceWaterHeight <  avgSurfLandHeight / numLandSurfaceSamples)
                        {
                            for (int vidx = 0;vidx < newVertices.Count;vidx++)
                            {
                                newVertices[vidx] = new Vector3(
                                    newVertices[vidx].x,
                                    (float)avgSurfaceWaterHeight + builder.waterHeightOffset + builder.terrainHeight/2,
                                    newVertices[vidx].z );
                            }
                            if (!(lowestSurfacePoint.x == 0 && lowestSurfacePoint.z == 0))
                                dbgLowestEdgePointsOut.Add( lowestSurfacePoint );

                            vertices.AddRange( newVertices );
                            tileIndices.AddRange( newIndices );
                        }
                    }
                }

                waterIslands.Add( (vertices, tileIndices, Vector2Int.zero) );
            }
            catch (Exception e) {  Debug.LogException(e); }
            return waterIslands;
        }

        // This is function is called in Task.Run().
        void ApplyWaterElevation( float[,] heights )
        {
            try
            {
                if (heights == null) // Possible if height from GeoTiff is not applied.
                {
                    heights = new float[builder.heightMapResolution+1, builder.heightMapResolution+1];
                }
                if (remappedLayerIndicesArray == null || !ContainsLayer( builder.waterLayerIdx ))
                {
                    return; // Exception in render vector tile or it has no water.
                }
                byte waterPixel    = GetRemappedLayerIdx( builder.waterLayerIdx );
                float heightMapRes = builder.heightMapResolution+1;
                int controlMapRes  = builder.controlResolution;
                float fx = controlMapRes / heightMapRes;
                int numRings = builder.numRingsToSearch;
                for (int y = 0;y<heightMapRes &&!cancelToken;y++)
                {
                    int ys = Mathf.FloorToInt(y * fx + 0.5f );
                    for (int x = 0;x<heightMapRes && !cancelToken;x++)
                    {
                        int xs = Mathf.FloorToInt(x * fx + 0.5f );
                        byte pixel = GetPixel(xs, ys);
                        if (pixel != waterPixel)
                            continue;

                        int numWaterPixels = 0;
                        bool stop = false;
                        byte prevPixel = waterPixel;
                        for (int c = 1;c < numRings && !stop;c++)
                        {
                            for (int y2 = ys-c;y2 <= ys+c &&!stop;y2++)
                            {
                                if (ShouldConsiderThisWaterPosition( xs-c, y2, controlMapRes, waterPixel, ref prevPixel, out byte nbPixel ) && nbPixel==waterPixel)
                                    numWaterPixels++;
                                //    else stop=true;
                            }
                            for (int y2 = ys-c;y2 <= ys+c && !stop;y2++)
                            {
                                if (ShouldConsiderThisWaterPosition( xs+c, y2, controlMapRes, waterPixel, ref prevPixel, out byte nbPixel ) && nbPixel==waterPixel)
                                    numWaterPixels++;
                                //      else stop = true;
                            }
                            for (int x2 = xs-c+1;x2 <= xs+c-1 && !stop;x2++)
                            {
                                if (ShouldConsiderThisWaterPosition( x2, ys-c, controlMapRes, waterPixel, ref prevPixel, out byte nbPixel ) && nbPixel==waterPixel)
                                    numWaterPixels++;
                                //       else stop = true;
                            }
                            for (int x2 = xs-c+1;x2 <= xs+c-1 && !stop;x2++)
                            {
                                if (ShouldConsiderThisWaterPosition( x2, ys+c, controlMapRes, waterPixel, ref prevPixel, out byte nbPixel ) && nbPixel==waterPixel)
                                    numWaterPixels++;
                                //        else stop = true;
                            }
                        }

                        float deltaHeightMeters = numWaterPixels * builder.deltaDepthWaterPixel;//, builder.waterDepth );
                        float deltaHeightNormalized = deltaHeightMeters / builder.terrainHeight;
                        heights[y, x] -= deltaHeightNormalized;
                    }
                }
            }
            catch (Exception e) { Debug.LogException( e ); }
        }

        bool IsValidWaterPosition( int x, int y, int resolution, byte waterPixel )
        {
            if (x < 0 || x >= resolution || y < 0 || y >= resolution)
            {
                var pixel = GetPixel( x, y );
                return pixel == waterPixel;
            }
            return false;
        }

        bool ShouldConsiderThisWaterPosition( int x, int y, int resolution, byte waterPixel, ref byte prevPixel, out byte pixel )
        {
            pixel = 255;
            if (x < 0 || x >= resolution || y < 0 || y >= resolution)
            {
                if (prevPixel==waterPixel)
                {
                    pixel = waterPixel;
                    return true;
                }
                return false;
            }
            pixel = GetPixel( x, y );
            prevPixel = pixel;
            return true;
        }

        // This function is executed in Task.Run().
        List<(Vector3, GameObject)> CreateFoliage( Vector3 tileCorner )
        {
            try
            {
                var foliageList = new List<(Vector3, GameObject)>();
                var candidatePixels = new Dictionary<byte, TerrainLayer2>();
                for (int l = 0;l < builder.layers.Count;l++)
                {
                    if (builder.layers[l].foliage == null || builder.layers[l].foliage.Count== 0 || builder.layers[l].foliageDensity == 0)
                    {
                        continue;
                    }

                    var pixel = GetRemappedLayerIdx( (byte)l );
                    if (pixel != 255)
                    {
                        candidatePixels.Add( pixel, builder.layers[l] );
                    }
                }
                int res   = builder.controlResolution;
                int shift = builder.foliageScanShift;
                float tileSize  = (float)RDUtils.CalcTileSizeRD(builder.zoom);
                System.Random r = new System.Random();
                for (int y = 0;y<res &&!cancelToken;y+=shift)
                {
                    for (int x = 0;x<res &&!cancelToken;x+=shift)
                    {
                        byte pixel = GetPixel(y,x);
                        if (!candidatePixels.ContainsKey( pixel ))
                            continue;
                        var layer  = candidatePixels[pixel];
                        bool spawn = r.NextDouble()*100 < 100 * layer.foliageDensity * builder.foliageGlobalDensity;
                        if (spawn)
                        {
                            var prefab   = layer.foliage[r.Next(layer.foliage.Count-1)];
                            var location = tileCorner + (new Vector3( (float)x / res, 0, (float)y/res ) * tileSize);
                            foliageList.Add( (location, prefab) );
                        }
                    }
                }
                return foliageList;
            }
            catch (Exception e) { Debug.LogException( e ); }
            return null;
        }

        void PlaceFoliage(List<(Vector3, GameObject)> foliageList)
        {
            GameObject foliageRoot = new GameObject("FoliageRoot");
            foliageList.ForEach( f =>
            {
                var location = f.Item1;
                if ((location + new Vector3( 0, 200, 0 )).Raycast( Vector3.down, 500, builder.groundLayer, out RaycastHit hit ))
                {
                    var prefab  = f.Item2;
                    if (prefab != null)
                    {
                        var foliage = Instantiate( prefab, hit.point, Quaternion.Euler( 0, UnityEngine.Random.Range( 0, 360 ), 0 ) );
                        foliage.transform.parent = foliageRoot.transform;
                        foliage.transform.up = hit.normal;
                        LODGroup LOD = foliage.GetComponent<LODGroup>();
                        if (LOD != null)
                        {
                            LOD.size = builder.foliageLODSize = 10;
                        }
                    }
                }
            } );
        }

        List<Mesh> CreateWaterIslands( List<(List<Vector3> vertices, List<int> indices, Vector2Int vtileOffs)> waterIslands )
        {
            var waterMeshes = new List<Mesh>();
            if (waterIslands == null)
                return waterMeshes;
            var tileSize = (float) builder.tileSize;
            var totalVertices = new List<Vector3>();
            var totalNormals  = new List<Vector3>();
            var totalUvs      = new List<Vector2>();
            var totalIndices  = new List<int>();
            for (int i = 0;i < waterIslands.Count;i++)
            {
                var vertices = waterIslands[i].vertices;
                var indices  = waterIslands[i].indices;
                var normals  = new List<Vector3>();
                var uvs      = new List<Vector2>();
                for (int j = 0;j<vertices.Count;j++)
                {
                    normals.Add( Vector3.up );
                    uvs.Add( new Vector2( vertices[j].x/tileSize, vertices[j].z/tileSize ) );
                }
                if (builder.waterCombineMeshes)
                {
                    for (int k = 0;k<indices.Count;k++)
                    {
                        indices[k] += totalVertices.Count;
                    }
                    totalVertices.AddRange( vertices );
                    totalNormals.AddRange( normals );
                    totalUvs.AddRange( uvs );
                    totalIndices.AddRange( indices );
                }
                else
                {
                    Mesh m = new Mesh();
                    m.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
                    m.SetVertices( vertices );
                    m.SetIndices( indices, MeshTopology.Triangles, 0 );
                    m.SetNormals( normals );
                    m.SetUVs( 0, uvs );
                    GameObject waterMesh = new GameObject($"WaterMesh_{waterIslands[i].vtileOffs.x}_{waterIslands[i].vtileOffs.y}");
                    waterMesh.layer = LayerMask.NameToLayer( "Water" );
                    waterMesh.transform.SetParent( transform, false );
                    waterMesh.AddComponent<MeshFilter>().sharedMesh = m;
                    var renderer = waterMesh.AddComponent<MeshRenderer>();
                    renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    renderer.enabled = false;
                    builder.water?.GetComponent<WaterSurface>().meshRenderers.Add( renderer );
                    waterMeshes.Add( m );
                }
            }
            if( builder.waterCombineMeshes )
            {
                Mesh m = new Mesh();
                m.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
                m.SetVertices( totalVertices );
                m.SetIndices( totalIndices, MeshTopology.Triangles, 0 );
                m.SetNormals( totalNormals );
                m.SetUVs( 0, totalUvs );
                GameObject waterMesh = new GameObject($"WaterMesh_{0}_{0}");
                waterMesh.layer = LayerMask.NameToLayer( "Water" );
                waterMesh.transform.SetParent( transform, false );
                waterMesh.AddComponent<MeshFilter>().sharedMesh = m;
                var renderer = waterMesh.AddComponent<MeshRenderer>();
                renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                renderer.enabled = false;
                builder.water?.GetComponent<WaterSurface>().meshRenderers.Add( renderer );
                waterMeshes.Add( m );
            }
            return waterMeshes;
        }

        void DoFinalSteps()
        {
            var waterMeshes = new List<Mesh>();
            var controlTex = CreateControlTexture();
            LinkLayersAndMaterial();
            float [,] heightData = builder.loadHeight ? normalizeHeightTask.Result : null;
            if ( heightData != null && terrainData != null )
            {
                terrainData.SetHeights( 0, 0, heightData );
            }
            if (createFoliageTask != null)
            {
                PlaceFoliage( createFoliageTask.Result );
            }
            if (createWaterTilesTask != null)
            {
                waterMeshes = CreateWaterIslands( createWaterTilesTask.Result );
            }
            if (!builder.isGenerating)
            {
                waterEdgeTilesKeys = new List<Vector2Int>();
                waterEdgeTilesValues = new List<int>();
                if (waterEdgeTiles != null) // If not created is null
                {
                    foreach (var edgeTile in waterEdgeTiles) // Put in two lists so it can be serialized.
                    {
                        waterEdgeTilesKeys.Add( edgeTile.Key );
                        waterEdgeTilesValues.Add( edgeTile.Value );
                    }
                }
                if (dbgWaterLowestEdgePoints != null)
                {
                    for (int k = 0;k < dbgWaterLowestEdgePoints.Count;k++)
                    {
                        var point = dbgWaterLowestEdgePoints[k];
                        if (builder.WaterType == WaterType.Islands)
                        {
                            var v0 = createWaterTilesTask.Result[k].Item1[0];
                            Debug.DrawLine( transform.position + point, transform.position + v0, Color.green, 30 );
                        }
                        Debug.DrawLine( transform.position + point, transform.position + point + Vector3.up*100, Color.red, 30 );
                        Debug.DrawLine( transform.position + point - new Vector3( -0.5f, 0, 0 ), transform.position + point - new Vector3( 0.5f, 0, 0 ), Color.blue, 30 );
                        Debug.DrawLine( transform.position + point - new Vector3( 0, 0, -0.5f ), transform.position + point - new Vector3( 0, 0, 0.5f ), Color.blue, 30 );
                    }
                }
            }
#if UNITY_EDITOR
            SaveToDisk( controlTex, waterMeshes );
#endif
  
            CancelAndClean();

            if (builder.isGenerating)
            {
                Debug.Log( $"Finished {builder.numStartedTiles}/{builder.numFinishedTiles} tile: {tileOrigin.x} {tileOrigin.y}" );
                GC.Collect();
            }
        }

#if UNITY_EDITOR
        void SaveToDisk( Texture2D controlTexture, List<Mesh> waterMeshes )
        {
            if ( controlTexture == null || terrainData == null || terrain == null )
            {
                return;
            }

            var areaName = SceneManager.GetActiveScene().name;
            var dirPath = Path.Combine( Application.dataPath, areaName, $"tiles_{builder.zoom}", $"tile_{tileOrigin.x}_{tileOrigin.y}").FS();
            if (!Directory.Exists( dirPath ) )
            { 
                Directory.CreateDirectory( dirPath );
            }

            for(int i=0; i<4; i++)
            {
                AssetDatabase.DisallowAutoRefresh();
            }

            var terrainPath    = Path.Combine( dirPath, "terrain.asset" ).ToAssetsPath(true).FS();
            var controlTexPath = Path.Combine( dirPath, "control_texture.asset" ).ToAssetsPath(true).FS();
            var materialPath   = Path.Combine( dirPath, "material.mat" ).ToAssetsPath(true).FS();
            AssetDatabase.CreateAsset( terrainData, terrainPath );
            AssetDatabase.CreateAsset( controlTexture, controlTexPath );
            AssetDatabase.CreateAsset( terrain.materialTemplate, materialPath );
            if (waterMeshes != null)
            {
                for (int i = 0;i < waterMeshes.Count;i++)
                {
                    var waterPath = Path.Combine( dirPath, $"water_mesh_{i}.mesh" ).ToAssetsPath().FS();
                    AssetDatabase.CreateAsset( waterMeshes[i], waterPath );
                }   
            }

            // Bundle all in a single prefab and remove TerrainTile script. Tile is now finished and can be loaded from a single prefab.
            var prefab  = Instantiate(gameObject);
            prefab.name = $"tile_{builder.zoom}_{tileOrigin.x}_{tileOrigin.y}";
            prefab.GetComponent<TerrainTile>().Destroy();
            var prefabPath = Path.Combine( dirPath, "tile.prefab" ).ToAssetsPath(true).FS();
            PrefabUtility.SaveAsPrefabAsset( prefab, prefabPath, out _ );

            if ( !builder.showTilesInEditor)
            {
                gameObject.Destroy();
            }

            prefab.Destroy();
        }
#endif

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        byte GetPixel( int x, int y )
        {
            var res   = builder.controlResolution;
            var addr  = (y*res+x);
            var shift = addr%2;
            var pixel = controlData[addr/2];
            return (byte)((pixel >> (shift*4)) & 15);
        }

        /* Helper Functions. For instance if water is placed at index 0, place a 0. If grass is at 1, insert 1. */
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool ContainsLayer(byte layerIndexInInspector)
        {
            return remappedLayerIndicesArray.Contains( layerIndexInInspector );
        }

        /* Helper Functions. For instance if water is placed at index 0, place a 0. If grass is at 1, insert 1. */
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public byte GetRemappedLayerIdx(byte layerIndexInInspector)
        {
            return (byte)Array.FindIndex( remappedLayerIndicesArray, p => layerIndexInInspector==p );
        }

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool HasWater()
        {
            return ContainsLayer( builder.waterLayerIdx );
        }
    }

#if UNITY_EDITOR
    [CustomEditor( typeof( TerrainTile ) )]
    [InitializeOnLoad]
    public class TerrainTileEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }
    }

#endif
}

#endif