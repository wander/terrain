using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
//using UnityEngine.Addressables; 

#if UNITY_EDITOR

namespace Wander
{
    [ExecuteInEditMode]
    public class TileValidator : MonoBehaviour
    {
        [ReadOnly]
        public Texture2D coverage;
        [ReadOnly]
        public string areaName;

        public bool slowDeepValiation;
        public Texture2D NL;

        public void SetGPSCoord( Dictionary<string, string> boundaryData )
        {
            areaName     = boundaryData["areaName"];

#if UNITY_EDITOR
            EditorUtility.SetDirty( this );
#endif
        }

        internal void ShowCoverage( int zoomLevel )
        {
            Color32 [] data = new Color32[4096*4096];
            var nlData = NL.GetPixels();

            double rdPerPixDst = (155126-140305)/140;
            double rdPerPixSrc = RDUtils.CalcTileSizeRD(0)/4096;
            double originXNL = 140305-1525*rdPerPixDst;
            double originYNL = 470623+1681*rdPerPixDst;

            // Copy NL card
            for (int y = 0;y < 4096;y++)
            {
                for (int x = 0;x < 4096;x++)
                {
                    double rdX = x * rdPerPixSrc + RDUtils.RDOriginX;
                    double rdY = y * rdPerPixSrc + RDUtils.RDOriginY;
                    int dstX = Mathf.RoundToInt( (float)((rdX - originXNL) / rdPerPixDst) );
                    int dstY = Mathf.RoundToInt( (float)((originYNL - rdY) / rdPerPixDst) );
                    if ( dstX >= 0 && dstX < NL.width && dstY >= 0 && dstY < NL.height )
                    {
                        data[y*4096+x] = nlData[(NL.height-dstY-1)*NL.width+dstX];
                    }
                }
            }

            if (Directory.Exists( Path.Combine( Application.dataPath, $"{areaName}/tiles_{zoomLevel}" ) ))
            {
                var tiles = Directory.GetDirectories( Path.Combine( Application.dataPath, $"{areaName}/tiles_{zoomLevel}" ) );
                for (int j = 0;j < tiles.Length;j++)
                {
                    try
                    {
                        int tileX = int.Parse( tiles[j].Split('_')[2] );
                        int tileY = int.Parse( tiles[j].Split('_')[3] );
                        int sc = 4096 / (1<<zoomLevel);
                        tileX *= sc;
                        tileY *= sc;
                        for (int y = 0;y < sc;y++)
                        {
                            for (int x = 0;x < sc;x++)
                            {
                                data[(tileY+y) * 4096 + tileX+x] = new Color32( 255, 0, 0, 255 );
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        Debug.LogException( e );
                    }
                }
            }

            coverage = new Texture2D( 4096, 4096, TextureFormat.RGBA32, false, true );
            coverage.SetPixelData( data, 0, 0 );
            coverage.Apply();
        }

        internal void LogIfInvalid( bool log, bool valid, string txt, int zoom, int x, int y, ref bool anyInvalid )
        {
            if (!valid)
            {
                if (log)
                {
                    Debug.LogWarning( $"{txt} missing for zoom {zoom}, tile {x} {y}" );
                }
                anyInvalid = true;
            }
        }

        internal void Validate( bool log, bool deleteInvalid )
        {
            var path = Path.Combine( Application.dataPath, areaName );
            if (!Directory.Exists( path ))
                return;
            var zoomLevels = Directory.GetDirectories( path );
            for (int i = 0; i < zoomLevels.Length; i++)
            {
                try
                {
                    int zoom  = int.Parse( zoomLevels[i].Split('_').Last() );
                    var tiles = Directory.GetDirectories( zoomLevels[i] );
                    for (int j = 0; j < tiles.Length; j++)
                    {
                        try
                        {
                            int tileX = int.Parse( tiles[j].Split('_')[2] );
                            int tileY = int.Parse( tiles[j].Split('_')[3] );

                            var tex  = File.Exists( Path.Combine( tiles[j], "control_texture.asset" ) );
                            var mat  = File.Exists( Path.Combine( tiles[j], "material.mat" ) );
                            var ter  = File.Exists( Path.Combine( tiles[j], "terrain.asset" ) );
                            var tile = File.Exists( Path.Combine( tiles[j], "tile.prefab" ) );

                            var texMeta  = File.Exists( Path.Combine( tiles[j], "control_texture.asset.meta" ) );
                            var matMeta  = File.Exists( Path.Combine( tiles[j], "material.mat.meta" ) );
                            var terMeta  = File.Exists( Path.Combine( tiles[j], "terrain.asset.meta" ) );
                            var tileMeta = File.Exists( Path.Combine( tiles[j], "tile.prefab.meta" ) );

                            bool anyInvalid = false;
                            LogIfInvalid( log, tex, "control_texture", zoom, tileX, tileY, ref anyInvalid );
                            LogIfInvalid( log, mat, "material", zoom, tileX, tileY, ref anyInvalid );
                            LogIfInvalid( log, ter, "terrain", zoom, tileX, tileY, ref anyInvalid );
                            LogIfInvalid( log, tile, "tile", zoom, tileX, tileY, ref anyInvalid );

                            LogIfInvalid( log, texMeta, "meta for control_texture", zoom, tileX, tileY, ref anyInvalid );
                            LogIfInvalid( log, matMeta, "meta for material", zoom, tileX, tileY, ref anyInvalid );
                            LogIfInvalid( log, terMeta, "meta for terrain", zoom, tileX, tileY, ref anyInvalid );
                            LogIfInvalid( log, tileMeta, "meta for tile", zoom, tileX, tileY, ref anyInvalid );

                            if (!anyInvalid && slowDeepValiation)
                            {
                                var prefabPath = Path.Combine( tiles[j], "tile.prefab" ).ToAssetsPath( true );
                                var prefab = AssetDatabase.LoadAssetAtPath<GameObject>( prefabPath );
                                if (prefab != null)
                                {   
                                    var go = Instantiate( prefab );
                                    if (go != null)
                                    {
                                        var terr = go.GetComponent<Terrain>();
                                        if ( terr != null )
                                        {
                                            if (terr.terrainData != null)
                                                goto L10n;
                                                
                                        }
                                    }
                                }
                                anyInvalid = true;
                            }

                            L10n:
                            if (anyInvalid && deleteInvalid)
                            {
                                File.Delete( tiles[j]+".meta" );
                                Directory.Delete( tiles[j], true );
                            }
                        }
                        catch ( Exception e )
                        {
                            Debug.LogException( e );
                        }
                    }
                }
                catch ( Exception e )
                {
                    Debug.LogException( e );
                }
            }
        }
    }
     
    [CustomEditor( typeof( TileValidator ) )]
    [InitializeOnLoad]
    public class TileValidatorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var valildator = target as TileValidator;
            DrawDefaultInspector();

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Validate" ))
                {
                    valildator.Validate( true, false );
                }
                if (GUILayout.Button( "Delete invalid" ))
                {
                    valildator.Validate( false, true );
                }
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Show 12" ))
                {
                    valildator.ShowCoverage( 12 );
                }
                if (GUILayout.Button( "Show 10" ))
                {
                    valildator.ShowCoverage( 10 );
                }
                if (GUILayout.Button( "Show 7" ))
                {
                    valildator.ShowCoverage( 7 );
                }
            }
            GUILayout.EndHorizontal();
        }
    }

}

#endif