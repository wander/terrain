using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using static Wander.TileStreamer;
using System.Threading.Tasks;
using Unity.Collections.LowLevel.Unsafe;
using System.Collections;






#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;

namespace Wander
{
    public class TerrainRawTile
    {
        public double originRDX;
        public double originRDY;
        public Vector2Int tileOrigin;    // Based on requested zoom.
        public Vector2Int vTileOrigin;   // Based on zoomlevel 12 (only available vTile).
        public List<(VectorTile, Vector2Int)> vTiles;
        
    }

    public class TerrainGeneratorRaw : MonoBehaviour
    {
        [Header("Generate stats")]
        [ReadOnly] public int numDownloads;
        [ReadOnly] public int numTasks;

        [Header("Area")]
        [ReadOnly] public double originRDX;
        [ReadOnly] public double originRDY;
        [ReadOnly] public double tileSize;
        [ReadOnly] public double offsetX;
        [ReadOnly] public double offsetY;
        [ReadOnly] public double boundsSize;
        [ReadOnly] public int vTileResolution;
        [ReadOnly] public int numVtilesPerTile;
        [ReadOnly] public int numVtilesPerWide;
        [ReadOnly] public int totalNumVTiles;
        [ReadOnly] public int vTileZoom = 12;
        [ReadOnly] public List<List<string>> layerNamesList;

        [Header("General")]
        public string  areaName             = "Steenbergen";
        public Vector2 originWGS84          = new Vector2(51.985365f, 5.664941f); // Copy directly from Google maps. This is Forum on Campus WUR.
        public int numRings                 = 1;
        public int zoom                     = 12;
        public int controlResolution        = 4096;
        public int heightMapResolution      = 512;
        public bool loadHeight              = true;
        public bool filterHeight            = true;
        public float terrainHeight          = 200;

        [Header("QueueLimits")]
        public int numActiveDownloads       = 8;
        public int numActiveTasks           = 8;

        private EditorCoroutine generateRoutine;
        private EditorCoroutine checkDownloadsRoutine;
        private EditorCoroutine checkTasksRoutine;
        private List<Task> tasks = new();
        private List<Func<(bool started, bool finished, Action starter)>> downloads = new();

        // Always the same.
        private OpenTK.Mathematics.Vector3d[] vTileClippingVertices;
        private int[] vTileClippingIndices;

        internal void Cancel()
        {
            if (generateRoutine!=null) 
                EditorCoroutineUtility.StopCoroutine( generateRoutine );
            if (checkDownloadsRoutine!=null)
                EditorCoroutineUtility.StopCoroutine( checkDownloadsRoutine );
            if (checkTasksRoutine!=null)
                EditorCoroutineUtility.StopCoroutine( checkTasksRoutine );

            generateRoutine = null;
            checkDownloadsRoutine = null;
            checkTasksRoutine = null;
            downloads = new();
            tasks = new();
        }

        void CheckPreliminaries()
        {
            tileSize = RDUtils.CalcTileSizeRD( zoom );
            RDUtils.GPS2RD( originWGS84.x, originWGS84.y, out originRDX, out originRDY );

            zoom = Mathf.Min( zoom, vTileZoom ); // It has no point to make a smaller tile than the min vector tile size (which is 12).
            numVtilesPerWide = (1 << (vTileZoom-zoom));
            vTileResolution  = controlResolution / numVtilesPerWide;
            numVtilesPerTile = numVtilesPerWide * numVtilesPerWide;

            if (controlResolution % vTileResolution != 0)
            {
                throw new InvalidOperationException( "Invalid vtile resolution" );
            }

            if (controlResolution % 128 != 0)
            {
                throw new InvalidOperationException( "Control resolution must be dividable by 128" );
            }

            if (controlResolution % heightMapResolution != 0)
            {
                throw new InvalidOperationException( "Control resolution must be dividable by heigtMapResolution, e.g. 4096 and 128" );
            }

            vTileClippingVertices = new[]
            {
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(-0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, -0.5),
                new OpenTK.Mathematics.Vector3d(0.5, 0.5, 0.5),
                new OpenTK.Mathematics.Vector3d(0.5, -0.5, 0.5),
            };

            float vTileSize = (float)RDUtils.CalcTileSizeRD( vTileZoom );
            for (int i = 0;i < vTileClippingVertices.Length;i++)
            {
                vTileClippingVertices[i] *= vTileSize;
                vTileClippingVertices[i] += new OpenTK.Mathematics.Vector3d( vTileSize/2, 0, vTileSize/2 );
            }

            vTileClippingIndices = new[] {
                0,2,3,
                0,3,1,
                8,4,5,
                8,5,9,
                10,6,7,
                10,7,11,
                12,13,14,
                12,14,15,
                16,17,18,
                16,18,19,
                20,21,22,
                20,22,23,
            };
        }

        public void SyncCoords()
        {
            var sceneObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            var boundaryData = new Dictionary<string, string>();

            tileSize = RDUtils.CalcTileSizeRD( zoom );
            boundaryData["gpsx"] = originWGS84.x.ToString();
            boundaryData["gpsy"] = originWGS84.y.ToString();
            boundaryData["zoom"] = zoom.ToString();
            boundaryData["tileSize"] = RDUtils.CalcTileSizeRD( zoom ).ToString();
            boundaryData["numRings"] = numRings.ToString();

            double rdX, rdY;
            RDUtils.GPS2RD( originWGS84.x, originWGS84.y, out rdX, out rdY );
            var requestedCentreTile   = RDUtils.RD2Tile( rdX, rdY, zoom );
            var requestedCentreTileRD = RDUtils.Tile2RD( requestedCentreTile, zoom );
            (double x, double y) offset = ( requestedCentreTileRD.rdX - rdX, requestedCentreTileRD.rdY - rdY );
            offsetX = offset.x;
            offsetY = offset.y;
            boundaryData["offsetx"] = offsetX.ToString();
            boundaryData["offsety"] = offsetY.ToString();
            
            boundsSize  = (numRings*2+1)*tileSize; // (NumRings*2+1) is nTiles wide.
            boundaryData["boundsSize"] = boundsSize.ToString();
            boundaryData["areaName"] = areaName;

            foreach ( var sceneObject in sceneObjects )
            {
                sceneObject.SendMessage( "SetGPSCoord", boundaryData, SendMessageOptions.DontRequireReceiver ); 
            }
#if UNITY_EDITOR
            EditorUtility.SetDirty( this );
#endif
        }

        static string GetPDOKVectorTileUrl( int tileX, int tileY, int zoom )
        {
            return $"https://api.pdok.nl/lv/bgt/ogc/v1_0/tiles/NetherlandsRDNewQuad/{zoom}/{tileY}/{tileX}?f=mvt";
        }

        public void Generate()
        {
            CheckPreliminaries();

            checkDownloadsRoutine = EditorCoroutineUtility.StartCoroutine( CheckDownloads(), this );

            var requestedCentreTile = RDUtils.RD2Tile( originRDX, originRDY, zoom );
            GenerateTile( requestedCentreTile, 0, 0 );
            for (int c = 1;c <= numRings;c++)
            {
                for (int y2 = -c;y2 <= c;y2++) GenerateTile( requestedCentreTile, -c, y2 );
                for (int y2 = -c;y2 <= c;y2++) GenerateTile( requestedCentreTile, c, y2 );
                for (int x2 = -c+1;x2 <= c-1;x2++) GenerateTile( requestedCentreTile, x2, c );
                for (int x2 = -c+1;x2 <= c-1;x2++) GenerateTile( requestedCentreTile, x2, -c );
            }
        }

        void GenerateTile( Vector2Int centre, int tx, int ty )
        {
            var coord = centre + new Vector2Int( tx, -ty);
            if (coord.x < 0 || coord.y < 0 || coord.x >= (1<<zoom) || coord.y >= (1<<zoom))
            {
                // Skip outside bounds tiles.
                return;
            }
            TerrainRawTile tile = new();
            var vTileSize    = RDUtils.CalcTileSizeRD( 12 );
            tile.tileOrigin  = coord;
            tile.originRDX   = RDUtils.Tile2RD( coord, zoom ).rdX;
            tile.originRDY   = RDUtils.Tile2RD( coord, zoom ).rdY;
            tile.vTileOrigin = RDUtils.RD2Tile( tile.originRDX + vTileSize/2, tile.originRDY + vTileSize/2, 12 );
            for (int y = 0; y < numVtilesPerWide; y++)
            {
                for (int x = 0; x < numVtilesPerWide; x++)
                {
                    var url   = GetPDOKVectorTileUrl(tile.vTileOrigin.x + x , tile.vTileOrigin.y - y, 12);
                    var vTile = VectorTileLoader.LoadFromUrl( url, false );
                    tile.vTiles.Add( (vTile, new Vector2Int( x, y )) );
                    downloads.Add( () => (vTile.DownloadStarted, vTile.Finished, () => vTile.StartDownload()) );
                }
            }
        }

        IEnumerator CheckDownloads()
        {
            while (true)
            {
                int nActive = 0;
                downloads.RemoveAll( ( dl ) =>
                {
                    var dlStatus = dl();
                    if (!dlStatus.finished && dlStatus.started)
                    {
                        nActive++;
                    }
                    return dlStatus.finished;
                } );
                int j = 0;
                int nAdditionalStarted = 0;
                for (int i = 0; i < numActiveDownloads-nActive; i++)
                {
                    for (; j< downloads.Count; j++)
                    {
                        var dlStatus = downloads[j]();
                        if (!dlStatus.started)
                        {
                            dlStatus.starter();
                            nAdditionalStarted++;
                            j++;
                            break;
                        }
                    }
                }
                numDownloads = nActive + nAdditionalStarted;
                yield return null;
            }
        }

        IEnumerator CheckTasks()
        {
            while (true)
            {
                tasks.RemoveAll( t => t.IsCompleted );
                yield return null;
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.magenta;
            float tileSize = (float)RDUtils.CalcTileSizeRD(zoom);
            Gizmos.DrawWireCube( new Vector3( (float)offsetX, 0, (float)offsetY ) + new Vector3( tileSize, 0, tileSize )/2, new Vector3( (float)boundsSize, 100, (float) boundsSize ) );
        }
    }

#if UNITY_EDITOR
    [CustomEditor( typeof( TerrainGeneratorRaw ) )]
    [InitializeOnLoad]
    public class TerrainGeneratorRawEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            TerrainGeneratorRaw generator = target as TerrainGeneratorRaw;

            //EditorGUILayout.HelpBox( "Max detail is: Zoom 12.", MessageType.Info, true );
            EditorGUILayout.HelpBox( "Max detail is: Zoom 12.\nLook up a location in Google maps and copy GPS coordinates in Origin WGS84 X, Y" +
                                     "\nIf you have forgotten a previously entered GPS coord, they don't need to match exactly.", MessageType.Info, true );

            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Generate" ))
                {
                    generator.SyncCoords();
                    generator.Generate();
                }
                if (GUILayout.Button( "Sync coords" ))
                {
                    generator.SyncCoords();
                }
                if (GUILayout.Button( "Cancel" ))
                {
                    generator.Cancel();
                }
            }
            GUILayout.EndHorizontal();

            DrawDefaultInspector();
        }
    }

#endif
}

#endif