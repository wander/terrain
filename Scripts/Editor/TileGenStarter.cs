using UnityEngine;

namespace Wander
{
    public class TileGenStarter : MonoBehaviour
    {
        public static void StartIt()
        {
            print( "IsBatch mode: " + Application.isBatchMode );
            var go = GameObject.Find( "TerrainGenerator_11" );
            go.GetComponent<TerrainBuilder>().SyncCoords();
            go.GetComponent<TerrainBuilder>().Build();
            print( "Build was Clicked" );
        }
    }

}
