#ifndef UNPACKCONTROLPIXEL
#define UNPACKCONTROLPIXEL


void GetControlPixel_float(float2 UV, float4 pix, float resolution, out float Result)
{
	uint x = (int) floor((1-UV.x)*resolution);
	Result = pix[x%4];
}

#endif