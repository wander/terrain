using UnityEditor;
using UnityEngine;

namespace Wander
{
    [ExecuteInEditMode()]
    public class DebugCGSIntersections : MonoBehaviour
    {
        public GameObject left;
        public GameObject right;
        public CSG.Boolean operation;
        public Material mat;
        public bool swapLeftRight;

        public void Do()
        {
            var a = left;
            var b = right;
            if (swapLeftRight)
            {
                a = right;
                b = left;
            }
            var res = CSG.BooleanOperation2( a.GetComponent<MeshFilter>().sharedMesh, b.GetComponent<MeshFilter>().sharedMesh, 0, 0, a.transform.localToWorldMatrix, b.transform.localToWorldMatrix, operation );
            GameObject go = new GameObject("result");
            go.AddComponent<MeshRenderer>().sharedMaterial = mat;
            go.AddComponent<MeshFilter>().sharedMesh = res.mesh;
            go.transform.position = res.offset;
           
        }

        private void OnDrawGizmos()
        {
        }
    }

#if UNITY_EDITOR
    [CustomEditor( typeof( DebugCGSIntersections ) )]
    [InitializeOnLoad]
    public class DebugCGSIntersectionsEditor : Editor
    {
        static DebugCGSIntersectionsEditor()
        {
        }

        public override void OnInspectorGUI()
        {
            DebugCGSIntersections settings = (DebugCGSIntersections)target;


            GUILayout.BeginHorizontal();
            {
                if (GUILayout.Button( "Do" ))
                {
                    settings.Do();

                }
            }
            GUILayout.EndHorizontal();

            DrawDefaultInspector();

        }
    }
#endif

}