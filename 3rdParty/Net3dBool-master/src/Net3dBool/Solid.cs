﻿/*
The MIT License (MIT)

Copyright (c) 2014 Sebastian Loncar

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

See:
D. H. Laidlaw, W. B. Trumbore, and J. F. Hughes.
"Constructive Solid Geometry for Polyhedral Objects"
SIGGRAPH Proceedings, 1986, p.161.

original author: Danilo Balby Silva Castanheira (danbalby@yahoo.com)

Ported from Java to C# by Sebastian Loncar, Web: http://www.loncar.de
Project: https://github.com/Arakis/Net3dBool

Optimized and refactored by: Lars Brubaker (larsbrubaker@matterhackers.com)
Project: https://github.com/MatterHackers/agg-sharp (an included library)
*/

using System;
using OpenTK.Mathematics;

namespace Net3dBool
{
    /// <summary>
    /// Class representing a 3D solid.
    /// </summary>
    public class Solid
    {
        /** array of indices for the vertices from the 'vertices' attribute */
        protected int[] Indices;
        /** array of points defining the solid's vertices */
        protected Vector3d[] Vertices;


        public Solid()
        {
            SetInitialFeatures();
        }

        public Solid(Vector3d[] vertices, int[] indices)
            : this()
        {
            SetData(vertices, indices);
        }

        protected void SetInitialFeatures()
        {
            Vertices = new Vector3d[0];
            Indices = new int[0];
        }

        public Vector3d[] GetVertices()
        {
            return Vertices;
        }

        public int[] GetIndices()
        {
            return Indices;
        }

        public bool IsEmpty => Indices.Length == 0;

        public void SetData(Vector3d[] vertices, int[] indices)
        {
            Vertices = vertices;
            Indices = indices;
        }

        public void Translate(double dx, double dy, double dz)
        {
            if (dx != 0 || dy != 0 || dz != 0)
            {
                for (int i = 0; i < Vertices.Length; i++)
                {
                    Vertices[i].X += dx;
                    Vertices[i].Y += dy;
                    Vertices[i].Z += dz;
                }
            }
        }

        public void Rotate(double dx, double dy)
        {
            double cosX = Math.Cos(dx);
            double cosY = Math.Cos(dy);
            double sinX = Math.Sin(dx);
            double sinY = Math.Sin(dy);

            if (dx != 0 || dy != 0)
            {
                //get mean
                Vector3d mean = GetMean();

                double newX, newY, newZ;
                for (int i = 0; i < Vertices.Length; i++)
                {
                    Vertices[i].X -= mean.X;
                    Vertices[i].Y -= mean.Y;
                    Vertices[i].Z -= mean.Z;

                    //x rotation
                    if (dx != 0)
                    {
                        newY = Vertices[i].Y * cosX - Vertices[i].Z * sinX;
                        newZ = Vertices[i].Y * sinX + Vertices[i].Z * cosX;
                        Vertices[i].Y = newY;
                        Vertices[i].Z = newZ;
                    }

                    //y rotation
                    if (dy != 0)
                    {
                        newX = Vertices[i].X * cosY + Vertices[i].Z * sinY;
                        newZ = -Vertices[i].X * sinY + Vertices[i].Z * cosY;
                        Vertices[i].X = newX;
                        Vertices[i].Z = newZ;
                    }

                    Vertices[i].X += mean.X;
                    Vertices[i].Y += mean.Y;
                    Vertices[i].Z += mean.Z;
                }
            }
        }

        public void Scale(double dx, double dy, double dz)
        {
            for (int i = 0; i < Vertices.Length; i++)
            {
                Vertices[i].X *= dx;
                Vertices[i].Y *= dy;
                Vertices[i].Z *= dz;
            }
        }

        public Vector3d GetMean()
        {
            Vector3d mean = new Vector3d();
            for (int i = 0; i < Vertices.Length; i++)
            {
                mean.X += Vertices[i].X;
                mean.Y += Vertices[i].Y;
                mean.Z += Vertices[i].Z;
            }
            mean.X /= Vertices.Length;
            mean.Y /= Vertices.Length;
            mean.Z /= Vertices.Length;

            return mean;
        }
    }
}

