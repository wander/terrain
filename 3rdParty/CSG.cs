using UnityEngine;
using Net3dBool;
using System.Linq;
using OpenTK.Mathematics;

namespace Wander
{
    
    public static class CSG
    {
        public enum Boolean
        {
            Union,
            Intersection,
            Difference
        }

        public static Solid BooleanOperationCore( Vector3d[] verts1, Vector3d[] verts2,  int[] indices1, int[] indices2, Boolean operation )
        {
            var s1 = new Solid(verts1, indices1);
            var s2 = new Solid(verts2, indices2);
            var modeller = new Net3dBool.BooleanModeller(s1, s2);
            Solid result = null;
            switch (operation)
            {
                case Boolean.Intersection:
                    result = modeller.GetIntersection();
                    break;
                case Boolean.Union:
                    result = modeller.GetUnion();
                    break;
                case Boolean.Difference:
                    result = modeller.GetDifference();
                    break;
            }
            return result;
        }

        public static (UnityEngine.Vector3[] verts, int[] indices, UnityEngine.Vector3 offset) BooleanOperation(Mesh m1, Mesh m2, int subMesh1, int subMesh2, Matrix4x4 mtr1, Matrix4x4 mtr2, Boolean operation, bool centrePivot = true )
        {
            var v1d = m1.vertices.Select( v => {
                v = mtr1.MultiplyPoint3x4(v);
                return new Vector3d( v.x, v.y, v.z ); 
            }).ToArray();
            var v2d = m2.vertices.Select( v =>
            {
                v = mtr2.MultiplyPoint3x4(v);
                return new Vector3d( v.x, v.y, v.z );
            }).ToArray();
            var s1 = new Solid(v1d, m1.GetIndices(subMesh1));
            var s2 = new Solid(v2d, m2.GetIndices(subMesh2));
            var modeller = new Net3dBool.BooleanModeller(s1, s2);
            Solid result = null;
            switch(operation)
            {
                case Boolean.Intersection:
                    result = modeller.GetIntersection();
                    break;
                case Boolean.Union:
                    result = modeller.GetUnion();
                    break;
                case Boolean.Difference:
                    result = modeller.GetDifference();
                    break;
            }
            UnityEngine.Vector3 ofs = UnityEngine.Vector3.zero;
            if(centrePivot && !result.IsEmpty)
            {
                var mean = result.GetMean();
                result.Translate( -mean.X, -mean.Y, -mean.Z );
                ofs = new UnityEngine.Vector3( (float)mean.X, (float)mean.Y, (float)mean.Z );
            }
            return (result.GetVertices().Select( v => new UnityEngine.Vector3( (float)v.X, (float)v.Y, (float)v.Z ) ).ToArray(), result.GetIndices(), ofs);
        }

        public static (Mesh mesh, UnityEngine.Vector3 offset) BooleanOperation2( Mesh m1, Mesh m2, int subMesh1, int subMesh2, Matrix4x4 mtr1, Matrix4x4 mtr2, Boolean operation,
            bool calcTangents = false, bool centrePivot = true )
        {
            var res = BooleanOperation( m1, m2, subMesh1, subMesh2, mtr1, mtr2, operation, centrePivot );
            Mesh m = new Mesh();
            m.vertices = res.verts;
            m.SetIndices( res.indices, MeshTopology.Triangles, 0 );
            m.RecalculateNormals();
            if ( calcTangents )
                m.RecalculateTangents();
            m.RecalculateBounds();
            return (m, res.offset);
        }
    }
}